package com.daw.javacol;

public interface Vehicle {
	
	void Drive();
	void Stop();
	void StartEngine();
	void TurnLeft();
	void TurnRight();
	void GoToITV();
}
