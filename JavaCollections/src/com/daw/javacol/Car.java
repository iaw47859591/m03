package com.daw.javacol;

public class Car implements Vehicle{
	String name;
	double price;
	String brand;
	
	// Constructors 
	
	public Car(String name, double price, String brand) {
		this.name = name;
		this.price = price;
		this.brand = brand;
	}

	
	// Getters & Setters
	
	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public double getPrice() {
		return price;
	}


	public void setPrice(double price) {
		this.price = price;
	}


	public String getBrand() {
		return brand;
	}


	public void setBrand(String brand) {
		this.brand = brand;
	}


	@Override
	public void Drive() {
		// TODO Auto-generated method stub
		System.out.println("Conduint el cotxe");
	}


	@Override
	public void Stop() {
		// TODO Auto-generated method stub
		System.out.println("Parant el cotxe");
	}


	@Override
	public void StartEngine() {
		// TODO Auto-generated method stub
		System.out.println("Engegant el cotxe");
	}


	@Override
	public void TurnLeft() {
		// TODO Auto-generated method stub
		System.out.println("Girant a l'esquerra");
	}


	@Override
	public void TurnRight() {
		// TODO Auto-generated method stub
		System.out.println("Girant a la dreta");
	}


	@Override
	public void GoToITV() {
		// TODO Auto-generated method stub
		
	}
	
	
}
