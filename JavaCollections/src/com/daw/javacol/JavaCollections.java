package com.daw.javacol;
import java.util.ArrayList;
import java.util.Iterator;

public class JavaCollections {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		ArrayList<String> l1 = new ArrayList<String>();
		
		System.out.println("------ Add/Set ------");
		
		l1.add("Gat");
		l1.add("Jaguar");
		
		System.out.println(l1.get(0));
		// System.out.println(l1.get(1));
		// System.out.println(l1.get(2)); 			Index out of bounds
		
		
		l1.set(1, "Guepard");
		System.out.println(l1.get(1));
		
		l1.add("Panter");
		l1.add("Lynx");

		System.out.println("------ Contains ------");

		
		if (l1.contains("Lynx")) {
			System.out.println("Lynx és present");
		} else {
			System.out.println("Lynx no trobat");
		}
		
		l1.add("Felí 2");
		
		System.out.println("------ For ------");
		for (int i = 0; i < l1.size(); i++) {
			System.out.println(l1.get(i));
		}
		
		System.out.println("------ Iterator ------");
		
		Iterator<String> i = l1.iterator();
		
		while (i.hasNext()) {
			System.out.println(i.next());
			
		}
		
		
		System.out.println("------ For (each) ------");
		
		for (String feli : l1) {
			System.out.println(feli);
		}
		
		System.out.println("------ Copy to array ------");
		String s[] = new String[l1.size()];
		l1.toArray(s);
		for (int j = 0; j < s.length; j++) {
			System.out.println(s[j]);
		}
		
		System.out.println("------ Cars ------");
		Car c1 = new Car("3Series", 20000, "BMW");
		Car c2 = new Car("3Series", 20000, "BMW");
		Car c3 = new Car("3Series", 20000, "BMW");
		
		System.out.println("-- Methods --");
		c1.TurnLeft();
		c2.Drive();
		c3.StartEngine();
		
		ArrayList<Car> c = new ArrayList<Car>();
		c.add(c1);
		c.add(c2);
		c.add(c3);
		
		System.out.println("-- Loop Cars 1 --");
		for (int k = 0; k < c.size(); k++) {
			System.out.println(c.get(k).brand + " " + c.get(k).name + " " + c.get(k).price);
		}
		
		System.out.println("-- Loop Cars 2 --");
		for (Car car : c) {
			System.out.println(car.brand + " " + car.name + " " + car.price);
		}
		
		
		
	}

}
