package m03.pau.lists;

public class ContactListEntry implements Comparable<ContactListEntry>{
	// Attributes
	private String firstName;
	private String lastName;
	private String surName;
	private int phoneNumber;
	private String email;
	private int cellPhoneNumber;
	
	
	// Constructors
	
	/**
	 * @param firstName
	 * @param lastName
	 * @param surName
	 * @param phoneNumber
	 * @param email
	 * @param cellPhoneNumber
	 */
	public ContactListEntry(String firstName, String lastName, String surName, int phoneNumber, String email,
			int cellPhoneNumber) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.surName = surName;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.cellPhoneNumber = cellPhoneNumber;
	}
	
	
	
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}



	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}



	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}



	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	// Compare To
		@Override
		public int compareTo(ContactListEntry c) {
			if (this.lastName.compareTo(c.lastName) == 0) {
				return this.firstName.compareTo(c.firstName);
			}
			return this.lastName.compareTo(c.lastName);
		}

	@Override
	public String toString() {
		return "ContactListEntry [firstName=" + firstName + ", lastName=" + lastName + ", surName=" + surName
				+ ", phoneNumber=" + phoneNumber + ", email=" + email + ", cellPhoneNumber=" + cellPhoneNumber + "]";
	}
	
	
	
}
