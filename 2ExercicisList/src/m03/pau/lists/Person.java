package m03.pau.lists;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;

public class Person implements Comparable<Person>{
	// Attributes
	private String dni; 		// The person's dni.
	private String firstName; 	// The person's first name.
	private String lastName;	// The person's lastName.
	private LocalDate birthday; // The person's birthday in LocalDate type.

	// Constructors

	/**
	 * Default constructor
	 * 
	 */
	public Person() {

	}
	
	/**
	 * Generates a person.
	 * 
	 * @param dni       the person's dni.
	 * @param firstName the person's first name.
	 * @param lastName  the person's last name.
	 * @param birthday  the person's birthday in LocalDate type.
	 */
	public Person(String dni, String firstName, String lastName, LocalDate birthday) {
		this.dni = dni;
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthday = birthday;
	}
	
	/**
	 * Generates a person.
	 * 
	 * @param dni       the person's dni.
	 * @param firstName the person's first name.
	 * @param lastName  the person's last name.
	 * @param birthday  the person's birthday string (dd/MM/yyyy) parsed to LocalDate type.
	 */
	public Person(String dni, String firstName, String lastName, String birthday) {
		this.dni = dni;
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthday = parseDate(birthday);
	}

	// Methods
	
	
	public static LocalDate parseDate(String date) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate d = LocalDate.parse(date, dtf);
		return d;
	}
	
	/**
	 * Calculate the person's age.
	 * 
	 * @return the person's age.
	 */
	public int age() {
		LocalDate now = LocalDate.now();
		Period difference = Period.between(this.birthday, now);
		return difference.getYears();
	}

	
	// Compare To
	@Override
	public int compareTo(Person p) {
		if (this.birthday.isAfter(p.birthday)) return -1;
		if (this.birthday.equals(p.birthday)) {
			if (this.lastName.compareTo(p.lastName) < 0) {
				return -1;
			}
		}
		return 1;
	}
	
	
	
	// To String 
	
	@Override
	public String toString() {
		return "Person [dni=" + dni + ", firstName=" + firstName + ", lastName=" + lastName + ", birthday=" + birthday
				+ ", age()=" + age() + "]";
	}
	
	
}
