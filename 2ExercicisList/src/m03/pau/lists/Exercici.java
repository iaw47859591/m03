package m03.pau.lists;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class Exercici {

	// Functions

	public static List<String> draw(List<String> baralla, int n){
		List<String> subList = baralla.subList(baralla.size()-n,baralla.size());
		
		List<String> ma = new ArrayList<String>(subList);
		
		baralla.subList(baralla.size()-n,baralla.size()).clear();

		return ma;
		
		}
	
	/**
	 * Generates a random number within a range of numbers.
	 * 
	 * @param min the minimum number to generate.
	 * @param max the maximum number to generate.
	 * @return a random number within min and max.
	 */
	public static int randomNumber(int min, int max) {
		int range = max - min + 1;
		int randomNumber = (int) ((Math.random() * range) + min);
		return randomNumber;
	}

	/**
	 * Return a list of persons under 18 age
	 * 
	 * @param l a list of persons
	 * @return a list of persons that are minors
	 */
	public static List menorsEdat(List<Person> l) {
		// Set the value to become adult.
		int minorAge = 18;
		// Generate the list that will be returned
		List<Person> minors = new ArrayList<Person>();
		// Loop through the list passed as parameter
		for (Person p : l) {
			// If the person is add it to the list.
			if (p.age() < minorAge)
				minors.add(p);
		}
		// Return the list of minors.
		return minors;
	}

	/**
	 * Return the closer person to be an adult.
	 * 
	 * @param l a list of persons sorted by age.
	 * @return a person.
	 */
	public static Person ultimMenor(List<Person> l) {
		// Set the value to become adult.
		int minorAge = 18;
		// Loop through the list inserted as parameter
		for (int i = 0; i < l.size(); i++) {
			// Return the person before the first adult
			if (l.get(i).age() >= minorAge) {
				return l.get(i - 1);
			}
		}
		return null;
	}

	public static void main(String[] args) {
		// EXERCICI 01
		System.out.println("------ Exercici 01 ------");
		// Create a new list
		List<Integer> listEx01 = new ArrayList<Integer>();

		// Traverse the arguments to insert them into the list
		for (String currentArgument : args) {
			// Parse the argument to integer and insert it to the list
			listEx01.add(Integer.parseInt(currentArgument));
		}
		// Show the list length
		System.out.printf("Elements llegits -> %d\n", listEx01.size());

		// Raise to the square every element from the array list
		int listSize = listEx01.size();
		for (int i = 0; i < listSize; i++) {
			listEx01.set(i, (listEx01.get(i) * listEx01.get(i)));
		}

		// Remove the number greaters than 100 with Iterator
		Iterator<Integer> iteratorLlist = listEx01.iterator();
		while (iteratorLlist.hasNext()) {
			if (iteratorLlist.next() > 100)
				iteratorLlist.remove();
		}

		// Sort the list with the Collection class
		Collections.sort(listEx01);

		// Show the list numbers
		for (Integer currentNumber : listEx01) {
			System.out.println(currentNumber);
		}

		// EXERCICI 02
		System.out.println("\n------ Exercici 02 ------");

		// Create a new list
		List<Integer> listEx02 = new ArrayList<Integer>();

		// Add random numbers to the second list
		int numbersToInsert = 5; // Times that a number will be generated randomly
		int minimum = -1000;
		int maximmum = 1000;
		// Array to save the generated random numbers.
		int[] generatedNumbers = new int[numbersToInsert];
		// Generate a random number and insert it into the second list
		for (int i = 0; i < numbersToInsert; i++) {
			generatedNumbers[i] = randomNumber(minimum, maximmum);
			listEx02.add(generatedNumbers[i]);
		}

		// Add all the elements from the second list into the first
		listEx01.addAll(listEx02);

		// Check if the elements are well inserted.
		int counterPositions = 0;
		// Loop checking if the first list contains the numbers from the generated
		// numbers
		while (counterPositions < listEx02.size() && listEx01.contains(generatedNumbers[counterPositions])) {
			counterPositions++;
		}
		// Print the checking result
		String containsCheckingAnswer = listEx02.size() == counterPositions
				? "Tots els elements nous s'han afegit a la llista"
				: "NO Tots els elements s'han afegit a la llista";
		System.out.println(containsCheckingAnswer);

		// Remove the elements from the second list
		listEx02.clear();
		// Print the removal result
		String containsClearAnswer = listEx02.size() == 0 ? "La segona llista s'ha netejat correctament"
				: "La segona llista NO s'ha netejat correctament";
		System.out.println(containsClearAnswer);

		// EXERCICI 03
		System.out.println("\n------ Exercici 03 ------");

		// Create Person objects
		Person p1 = new Person("C-137", "Richard", "Sanchez", "02/12/1950");
		Person p2 = new Person("C-137", "Mortimer", "Smith", "14/04/2006");
		Person p3 = new Person("R-137", "Summer", "Smith", "26/07/2003");
		Person p4 = new Person("R-132", "Beth", "Smith (Sanchez)", "04/10/1986");
		Person p5 = new Person("C-132", "Jerry", "Smith", "01/04/1985");

		// Create a Person type list
		List<Person> personsList = new ArrayList<Person>();
		// Add persons to the list
		personsList.add(p1);
		personsList.add(p2);
		personsList.add(p3);
		personsList.add(p4);
		personsList.add(p5);

		// List the elements in personsList
		System.out.println("Llist de persones:");
		for (Person p : personsList) {
			System.out.println("\t" + p);
		}
		// Get the minors from the persons list
		List<Person> minorsList = menorsEdat(personsList);

		// List the minors list
		System.out.println("Llist de menors d'edat:");
		for (Person p : minorsList) {
			System.out.println("\t" + p);
		}

		// EXERCICI 04
		System.out.println("\n------ Exercici 04 ------");

		// Sort the list with an anon function.
		Collections.sort(personsList, new Comparator<Person>() {
			public int compare(Person p1, Person p2) {
				return p1.age() - p2.age();
			}
		});
		// Print the sorted list
		System.out.println("Llist de persones (ordenada per edat):");
		for (Person p : personsList) {
			System.out.println("\t" + p);
		}

		// Get the person closer to by an adult
		Person lastMinorPerson = ultimMenor(personsList);

		// Get the list of minors from the sorted list (0 to last minor person)
		List<Person> minorList2 = personsList.subList(0, (personsList.indexOf(lastMinorPerson) + 1));

		// Print the minors List 2
		System.out.println("Llist de menors d'edat:");
		for (Person p : minorList2) {
			System.out.println("\t" + p);
		}

		// EXERCICI 05
		System.out.println("\n------ Exercici 05 ------");
		// Clear the list
		personsList.clear();
		// A new person with same age
		Person p6 = new Person("C-137", "Evil", "Morty", "14/04/2006");
		// Add persons to the list
		personsList.add(p1);
		personsList.add(p2);
		personsList.add(p3);
		personsList.add(p4);
		personsList.add(p5);
		personsList.add(p6);
		// Print the list
		System.out.println("Llist de persones:");
		for (Person p : personsList) {
			System.out.println("\t" + p);
		}

		// Sort the list
		Collections.sort(personsList);

		// Print the sorted list
		System.out.println("Llist de persones (ordenada per data i cognom):");
		for (Person p : personsList) {
			System.out.println("\t" + p);
		}

		// EXERCICI 06
		System.out.println("\n------ Exercici 06 ------");

		// Create a list of contact list entries.
		List<ContactListEntry> cle = new ArrayList<ContactListEntry>();

		// Add contacts to the list
		cle.add(new ContactListEntry("Ana", "Torroja", "Fungairiño", 977123456, "atorroja@mecano.com", 666555444));
		cle.add(new ContactListEntry("Nacho", "Cano", "Andrés", 977456789, "ncano@mecano.com", 666333222));
		cle.add(new ContactListEntry("José Maria", "Cano", "Andrés", 977789123, "jcano@mecano.com", 666777888));

		// List the contact entry list
		System.out.println("Llist de contactes:");
		for (ContactListEntry c : cle) {
			System.out.println("\t" + c);
		}

		// Sort the cle list
		Collections.sort(cle);

		System.out.println("Llist de contactes (ordenada per cognom i nom:");
		for (ContactListEntry c : cle) {
			System.out.println("\t" + c);
		}

		// EXERCICI 07
		System.out.println("\n------ Exercici 07 ------");
		// Initial arrays
		String[] pals = {"cors", "piques", "diamants", "trèvols"};
		String[] cartes = {"As", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"};
		
		// Create the deck
		List<String> deck = new ArrayList<String>();
		
		// Initialize each element with an array list
		for (int i = 0; i < pals.length; i++) {
			for (int j = 0; j < cartes.length; j++) {
				deck.add("" + cartes[j] + " de " + pals[i]);
			}
		}
		
		// Shuffle the deck
		Collections.shuffle(deck);
		
		// Number of players and cards for player
		int numPlayers = 5;
		int cardsInHand = 5;
		
		for (int i = 0; i < numPlayers; i++) {			
		// Remove the shuffled cards.
		deck.subList(deck.size() - cardsInHand, deck.size()).clear();
		}
	}

}
