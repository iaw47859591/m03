package edt.pau.re1;

public class Programmer {
	// Attributes declaration
	String name = "john";
	String id = "1";
	int salary = 1000;

	public static void main(String[] args) {
		// Create an object (Programmer) "prog"
		Programmer prog = new Programmer();
		
		// Displaying attributes
		System.out.println("Name: " + prog.name);
		System.out.println("ID: " + prog.id);
		System.out.println("Salary: " + prog.salary);
		
		// Changing attributes
		prog.name = "Pau";
		prog.id = "A";
		prog.salary = 3500;
		
		// Displaying changed attributes
		System.out.println("Name: " + prog.name);
		System.out.println("ID: " + prog.id);
		System.out.println("Salary: " + prog.salary);		
	}

}
