package Exercici_reforç_de_1r;

import java.time.LocalDateTime;

public class Cuiner extends Tripulant {
	// Attributes
	private boolean serveiEnElPont; // If the "tripulant" is currently on service at the bridge is true.
	private String descripcióFeina;

	/**
	 * Default Constructor
	 */
	protected Cuiner() {

	}

	/**
	 * @param iD
	 * @param nom
	 * @param actiu
	 * @param dataAlta
	 * @param departament
	 * @param llocDeServei
	 * @param serveiEnElPont
	 * @param descripcióFeina
	 */
	protected Cuiner(String iD, String nom, boolean actiu, LocalDateTime dataAlta, int departament, int llocDeServei,
			boolean serveiEnElPont, String descripcióFeina) {
		super(iD, nom, actiu, dataAlta, departament, llocDeServei);
		this.serveiEnElPont = serveiEnElPont;
		this.descripcióFeina = descripcióFeina;
	}

	// Methods
	private String serveixEnElPont() {
		if (serveiEnElPont) {
			return "SI";
		}
		return "NO";
	}

	protected void imprimirDadesTripulant() {
		System.out.println("DADES TRIPULANT:");
		System.out.printf("\tBandol: %s\n", Tripulant.bandol);
		System.out.printf("\tID: %s\n", this.ID);
		System.out.printf("\tNom: %s\n", this.nom);
		System.out.printf("\tActiu: %b\n", this.actiu);
		System.out.printf("\tDepartament (de la clase Tripulan): %d\n", this.departament);
		System.out.printf("\tDepartament (de la clase IKSRotarranConstants): %s\n",
				IKSRotarranConstants.DEPARTAMENT[this.departament]);
		System.out.printf("\tLloc de servei (de la clase Tripulant): %d\n", getLlocDeServei());
		System.out.printf("\tLloc de servei (de la clase IKSRotarranConstants): %s\n",
				IKSRotarranConstants.LLOCS_DE_SERVEI[getLlocDeServei()]);
		System.out.printf("\tDescripció de la feina que fa: %s\n", this.descripcióFeina);
		System.out.printf("\tServeix en el pont: %s\n", serveixEnElPont());
		System.out.printf("\tData Alta: %s\n", formatDate(this.dataAlta));
	}

}
