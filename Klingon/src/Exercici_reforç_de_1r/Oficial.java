package Exercici_reforç_de_1r;

import java.time.LocalDateTime;

public class Oficial extends Tripulant {
	// Attributes
	private boolean serveiEnElPont; // If the "tripulant" is currently on service at the bridge is true.
	private String descripcióFeina;

	// Constructors

	/**
	 * Default Constructor
	 */
	protected Oficial() {

	}

	/**
	 * @param iD
	 * @param nom
	 * @param actiu
	 * @param dataAlta
	 * @param departament
	 * @param llocDeServei
	 * @param serveiEnElPont
	 * @param descripcióFeina
	 */
	protected Oficial(String iD, String nom, boolean actiu, LocalDateTime dataAlta, int departament, int llocDeServei,
			boolean serveiEnElPont, String descripcióFeina) {
		super(iD, nom, actiu, dataAlta, departament, llocDeServei);
		this.serveiEnElPont = serveiEnElPont;
		this.descripcióFeina = descripcióFeina;
	}

	// Methods
	private String serveixEnElPont() {
		if (serveiEnElPont) {
			return "SI";
		}
		return "NO";
	}

	protected void imprimirDadesTripulant() {
		System.out.println("DADES TRIPULANT:");
		System.out.printf("\tBandol: %s\n", Tripulant.bandol);
		System.out.printf("\tID: %s\n", this.ID);
		System.out.printf("\tNom: %s\n", this.nom);
		System.out.printf("\tActiu: %b\n", this.actiu);
		System.out.printf("\tDepartament (de la clase Tripulan): %d\n", this.departament);
		System.out.printf("\tDepartament (de la clase IKSRotarranConstants): %s\n",
				IKSRotarranConstants.DEPARTAMENT[this.departament]);
		System.out.printf("\tLloc de servei (de la clase Tripulant): %d\n", getLlocDeServei());
		System.out.printf("\tLloc de servei (de la clase IKSRotarranConstants): %s\n",
				IKSRotarranConstants.LLOCS_DE_SERVEI[getLlocDeServei()]);
		System.out.printf("\tDescripció de la feina que fa: %s\n", this.descripcióFeina);
		System.out.printf("\tServeix en el pont: %s\n", serveixEnElPont());
		System.out.printf("\tData Alta: %s\n", formatDate(this.dataAlta));
	}

	protected void saludar() {
		System.out.println("Hola des de la subclasse Oficial");
	}

	// Getters Setters

	protected boolean isServeiEnElPont() {
		return serveiEnElPont;
	}

	protected void setServeiEnElPont(boolean serveiEnElPont) {
		this.serveiEnElPont = serveiEnElPont;
	}

	protected String getDescripcióFeina() {
		return descripcióFeina;
	}

	protected void setDescripcióFeina(String descripcióFeina) {
		this.descripcióFeina = descripcióFeina;
	}

	@Override
	public String toString() {
		return "Oficial [serveiEnElPont=" + serveiEnElPont + ", descripcióFeina=" + descripcióFeina + ", ID=" + ID
				+ ", nom=" + nom + ", actiu=" + actiu + ", dataAlta=" + dataAlta + ", departament=" + departament + "]";
	}

}
