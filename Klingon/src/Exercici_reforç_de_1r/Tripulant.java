package Exercici_reforç_de_1r;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Generates Tripulants objects
 * 
 * @author iaw47859591
 *
 */
public abstract class Tripulant {
	// Attributes
	protected static final String bandol = "Imperi Klingon";
	protected String ID;
	protected String nom;
	protected boolean actiu; // If the doctor discharges them then it will be false
	protected LocalDateTime dataAlta;
	protected int departament; // command(1), weapons (2), rudder and navigation (3),
								// engineering (4), science (5)
	private int llocDeServei; // bridge(1), engineering(2), kitchen(3), nursery(4), weapons room(5)

	// Constructor

	/**
	 * Default Constructor
	 */
	protected Tripulant() {

	}

	/**
	 * Construct a Tripulant
	 * 
	 * @param iD
	 * @param nom
	 * @param actiu
	 * @param dataAlta
	 * @param departament
	 * @param llocDeServei
	 */
	protected Tripulant(String iD, String nom, boolean actiu, LocalDateTime dataAlta, int departament,
			int llocDeServei) {
		ID = iD;
		this.nom = nom;
		this.actiu = actiu;
		this.dataAlta = dataAlta;
		this.departament = departament;
		this.llocDeServei = llocDeServei;
	}

	// Methods
	protected String formatDate(LocalDateTime dateToFormat) {
		DateTimeFormatter date_format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		return dateToFormat.format(date_format);
	}

	protected void imprimirDadesTripulant() {
		System.out.printf("Bandol: %s\n", Tripulant.bandol);
		System.out.printf("ID: %s\n", this.ID);
		System.out.printf("Nom: %s\n", this.nom);
		System.out.printf("Actiu: %s\n", this.actiu ? "Si" : "No");
		System.out.printf("Data Alta: %s\n", formatDate(this.dataAlta));
		System.out.printf("Departament: %d\n", IKSRotarranConstants.DEPARTAMENT[this.departament]);
		System.out.printf("Lloc de servei: %d\n", IKSRotarranConstants.LLOCS_DE_SERVEI[this.llocDeServei]);
	}

	protected void saludar() {
		System.out.println("Hola des de la superclasse Tripulant");
	}

	// Getters & Setters

	protected int getLlocDeServei() {
		return llocDeServei;
	}

	protected void setLlocDeServei(int llocDeServei) {
		this.llocDeServei = llocDeServei;
	}

	// Equals

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tripulant other = (Tripulant) obj;
		if (ID == null) {
			if (other.ID != null)
				return false;
		} else if (!ID.equals(other.ID))
			return false;
		return true;
	}

}
