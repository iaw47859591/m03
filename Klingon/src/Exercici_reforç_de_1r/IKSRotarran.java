package Exercici_reforç_de_1r;

import java.time.LocalDateTime;

public class IKSRotarran {

	/**
	 * Parse a string to return a Local Date Time object.
	 * 
	 * @param dateToParse a date in dd-mm-yyyy hh:mm string format.
	 * @return the date as a LocalDateTime object.
	 */
	public static LocalDateTime parseDate(String dateToParse) {
		// Split the date into Date and Time
		String[] dateTime = dateToParse.split(" ");
		String[] yearMonthDay = dateTime[0].split("-");
		String[] hourMinute = dateTime[1].split(":");
		// Split and pass to integer the parameters to transform the String date into
		// LocalDateTime object
		int year = Integer.parseInt(yearMonthDay[2]);
		int month = Integer.parseInt(yearMonthDay[1]);
		int dayOfMonth = Integer.parseInt(yearMonthDay[0]);
		int hour = Integer.parseInt(hourMinute[0]);
		int minute = Integer.parseInt(hourMinute[1]);
		// Generate a LocalDateTime object from the string date.
		LocalDateTime formatDate = LocalDateTime.of(year, month, dayOfMonth, hour, minute);
		// Return the object
		return formatDate;
	}

	public static void main(String[] args) {
		// Object capita
		Oficial capita = new Oficial("001-A", "Martok", true, parseDate("15-08-1954 00:01"), 1, 1, true,
				"Capitanejar la nau");
		Mariner mariner_02_03 = new Mariner("758-J", "Kurak", true, parseDate("26-12-1981 13:42"), 3, 1, true,
				"Mariner encarregat del timó i la navegació durant el 2n torn");

		// Preguntes

		System.out.println("Departament capita: " + capita.departament + "\n");
		// Es pot accedir perque l'atribut esta definit com a protected.

		// ERROR: System.out.println("Descripció feina capita: " +
		// capita.descripcióFeina);
		// No es pot accedir perque l'atribut esta definit com a private.

		System.out.println("Descripció feina capita: " + capita.getDescripcióFeina() + "\n");
		// He generat getters i setters i accedeixo a travès d'ells.

		capita.imprimirDadesTripulant();
		// Afegit el toString per a imprimir l'objecte amb estructura llegible.

		capita.departament = 10;
		// System.out.println("\nDepartament capita: " + capita.departament);

		System.out.println();
		Tripulant oficialDeTipusTripulant = new Oficial();
		Oficial oficialDeTipusOficial = new Oficial();
		oficialDeTipusTripulant.saludar();
		oficialDeTipusOficial.saludar();
		// Ambdos criden al saludar de oficial.

		System.out.println();
		System.out.println("L'objecte capita (té implementat el toString()): " + capita);
		System.out.println("L'objecte mariner_02_03 (NO té implementat el toString()): " + mariner_02_03);

		System.out.println();
		mariner_02_03.imprimirDadesTripulant();
	}

}
