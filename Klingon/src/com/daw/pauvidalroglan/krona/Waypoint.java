package com.daw.pauvidalroglan.krona;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Scanner;

import Llibreries.Cadena;

public class Waypoint {
	
	private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

	public static ComprovacioRendiment inicialitzarComprovacioRendiment() {
		ComprovacioRendiment comprovacioRendimentTmp = new ComprovacioRendiment();
		return comprovacioRendimentTmp;
	}

	public static ComprovacioRendiment comprovarRendimentInicialitzacio(int numObjACrear,
			ComprovacioRendiment comprovacioRendimentTmp) {
		// Variables declaration
		long starterTime;
		long endTime;
		
		// Date formatter pattern
		
		// ARRAY LIST
		// Get the starting nanoseconds
		starterTime = System.nanoTime();
		for (int i = 0; i < numObjACrear; i++) {
			comprovacioRendimentTmp.llistaArrayList
					.add(new Waypoint_Dades(0, "Òrbita de la Terra", comprovacioRendimentTmp.coordenadesTmp, true,
							LocalDateTime.parse("15-08-1954 00:01", formatter), null, null));
		}
		// Get the ending nanoseconds
		endTime = System.nanoTime();
		// Print the time in milisecons
		System.out.println("Temps per insertar " + numObjACrear + " waypoints a l'ArrayList: "
				+ Duration.ofNanos(endTime - starterTime).toMillis());

		// LINKED LIST
		// Get the starting nanoseconds
		starterTime = System.nanoTime();
		for (int i = 0; i < numObjACrear; i++) {
			comprovacioRendimentTmp.llistaLinkedList
					.add(new Waypoint_Dades(0, "Òrbita de la Terra", comprovacioRendimentTmp.coordenadesTmp, true,
							LocalDateTime.parse("15-08-1954 00:01", formatter), null, null));
		}
		// Get the ending nanoseconds
		endTime = System.nanoTime();
		// Print the time in milisecons
		System.out.println("Temps per insertar " + numObjACrear + " waypoints a l'ArrayList: "
				+ Duration.ofNanos(endTime - starterTime).toMillis());
		return comprovacioRendimentTmp;
	}

	public static ComprovacioRendiment comprovarRendimentInsercio(ComprovacioRendiment comprovacioRendimentTmp) {
		int centerList = Krona.OBJECTES_A_CREAR / 2;
		long starterTime;
		long endTime;
		

		// Add component FIRST position
		// Array
		starterTime = System.nanoTime();
		comprovacioRendimentTmp.llistaArrayList.add(0,
				new Waypoint_Dades(0, "Òrbita de la Terra", comprovacioRendimentTmp.coordenadesTmp, true,
						LocalDateTime.parse("15-08-1954 00:01", formatter), null, null));

		// Get the ending nanoseconds
		endTime = System.nanoTime();
		// Print the time in milisecons
		System.out.println("Temps per insertar 1 waypoint a la 1a positicó de l'ArrayList: "
				+ Duration.ofNanos(endTime - starterTime).getNano());

		// Linked
		starterTime = System.nanoTime();
		comprovacioRendimentTmp.llistaLinkedList
				.addFirst(new Waypoint_Dades(0, "Òrbita de la Terra", comprovacioRendimentTmp.coordenadesTmp, true,
						LocalDateTime.parse("15-08-1954 00:01", formatter), null, null));

		// Get the ending nanoseconds
		endTime = System.nanoTime();
		// Print the time in milisecons
		System.out.println("Temps per insertar 1 waypoint a la 1a positicó del LinkedList: "
				+ Duration.ofNanos(endTime - starterTime).getNano());

		// Add component MIDDLE position
		// Array
		starterTime = System.nanoTime();
		comprovacioRendimentTmp.llistaArrayList.add(centerList,
				new Waypoint_Dades(0, "Òrbita de la Terra", comprovacioRendimentTmp.coordenadesTmp, true,
						LocalDateTime.parse("15-08-1954 00:01", formatter), null, null));

		// Get the ending nanoseconds
		endTime = System.nanoTime();
		// Print the time in milisecons
		System.out.println("Temps per insertar 1 waypoint al mig (pos. " + centerList + ") positicó de l'ArrayList: "
				+ Duration.ofNanos(endTime - starterTime).getNano());

		// Linked
		starterTime = System.nanoTime();
		comprovacioRendimentTmp.llistaLinkedList.add(centerList,
				new Waypoint_Dades(0, "Òrbita de la Terra", comprovacioRendimentTmp.coordenadesTmp, true,
						LocalDateTime.parse("15-08-1954 00:01", formatter), null, null));

		// Get the ending nanoseconds
		endTime = System.nanoTime();
		// Print the time in milisecons
		System.out.println("Temps per insertar 1 waypoint al mig (pos. " + centerList + ") positicó del LinkedList: "
				+ Duration.ofNanos(endTime - starterTime).getNano());

		// Add component LAST position
		// Array
		starterTime = System.nanoTime();
		comprovacioRendimentTmp.llistaArrayList.add(comprovacioRendimentTmp.llistaArrayList.size(),
				new Waypoint_Dades(0, "Òrbita de la Terra", comprovacioRendimentTmp.coordenadesTmp, true,
						LocalDateTime.parse("15-08-1954 00:01", formatter), null, null));

		// Get the ending nanoseconds
		endTime = System.nanoTime();
		// Print the time in milisecons
		System.out.println("Temps per insertar 1 waypoint al final de l'ArrayList: "
				+ Duration.ofNanos(endTime - starterTime).getNano());
		// Linked
		starterTime = System.nanoTime();
		comprovacioRendimentTmp.llistaLinkedList
				.addLast(new Waypoint_Dades(0, "Òrbita de la Terra", comprovacioRendimentTmp.coordenadesTmp, true,
						LocalDateTime.parse("15-08-1954 00:01", formatter), null, null));

		// Get the ending nanoseconds
		endTime = System.nanoTime();
		// Print the time in milisecons
		System.out.println("Temps per insertar 1 waypoint al  al final del LinkedList: "
				+ Duration.ofNanos(endTime - starterTime).getNano());

		return comprovacioRendimentTmp;
	}

	public static ComprovacioRendiment modificarWaypoints(ComprovacioRendiment comprovacioRendimentTmp) {
		System.out.println("---- APARTAT 1 ----");
		List<Integer> idsPerArrayList = new ArrayList<Integer>();
		for (int i = 0; i < comprovacioRendimentTmp.llistaArrayList.size(); i++) {
			idsPerArrayList.add(i);
		}
		System.out.printf("S'ha inicialitzat la llista idsPerArrayList amb %d elements\n", idsPerArrayList.size());
		System.out.printf("El primer element té el valor: %d\nL'últim element té el valor: %d\n",
				idsPerArrayList.get(0), idsPerArrayList.get(idsPerArrayList.size() - 1));

		System.out.println("---- APARTAT 2 ----");
		for (Integer i : idsPerArrayList) {
			int waypointId = comprovacioRendimentTmp.llistaArrayList.get(i).getId();
			System.out.printf("ABANS DEL CANVI: comprovacioRendimentTmp.llistaArrayList.get(%d).getId(): %d\n", i,
					waypointId);

			comprovacioRendimentTmp.llistaArrayList.get(i).setId(i);

			waypointId = comprovacioRendimentTmp.llistaArrayList.get(i).getId();
			System.out.printf("DESPRÉS DEL CANVI: comprovacioRendimentTmp.llistaArrayList.get(%d).getId(): %d\n\n", i,
					waypointId);
		}

		System.out.println("---- APARTAT 3.1 (bucle for) ----");
		for (Waypoint_Dades w : comprovacioRendimentTmp.llistaArrayList) {
			System.out.printf("ID = %d, nom = %s\n", w.getId(), w.getNom());
		}

		System.out.println();
		System.out.println("---- APARTAT 3.1 (Iterator) ----");
		Iterator<Waypoint_Dades> iter = comprovacioRendimentTmp.llistaArrayList.iterator();
		while (iter.hasNext()) {
			Waypoint_Dades currentWaypoint = iter.next();
			System.out.printf("ID = %d, nom = %s\n", currentWaypoint.getId(), currentWaypoint.getNom());
		}

		System.out.println();
		System.out.println("---- APARTAT 4 ----");
		System.out.printf("Preparat per esborrar el contingut de listaLinkedList que té %d elements\n",
				comprovacioRendimentTmp.llistaLinkedList.size());

		comprovacioRendimentTmp.llistaLinkedList.clear();
		System.out.printf("Esborrada. Ara llistaLinkedList té %d elements\n",
				comprovacioRendimentTmp.llistaLinkedList.size());

		for (Waypoint_Dades w : comprovacioRendimentTmp.llistaArrayList) {
			comprovacioRendimentTmp.llistaLinkedList.add(w);
		}
		System.out.printf("Copiats els elements de llistaArrayList a listaLinkedList que té %d elements\n",
				comprovacioRendimentTmp.llistaLinkedList.size());

		System.out.println("---- APARTAT 5.1 (bucle for) ----");
		for (Waypoint_Dades w : comprovacioRendimentTmp.llistaArrayList) {
			if (w.getId() > 5) {
				w.setNom("Òrbita de Mart");
				System.out.printf("Modificat el waypoint amb id %d\n", w.getId());
			}
		}

		System.out.println();
		System.out.println("---- APARTAT 5.1 (comprovació) ----");
		for (int i = 0; i < comprovacioRendimentTmp.llistaArrayList.size(); i++) {
			System.out.printf("El waypoint amb id = %d té el nom = %s\n",
					comprovacioRendimentTmp.llistaArrayList.get(i).getId(),
					comprovacioRendimentTmp.llistaArrayList.get(i).getNom());
		}

		System.out.println();
		System.out.println("---- APARTAT 5.2 (Iterator) ----");
		Iterator<Waypoint_Dades> i = comprovacioRendimentTmp.llistaArrayList.iterator();

		while (i.hasNext()) {
			Waypoint_Dades w = i.next();
			if (w.getId() < 5) {
				w.setNom("Punt Lagrande entre la Terra i la Lluna");
				System.out.printf("Modificat el waypoint amb id = %d\n", w.getId());
			}

		}

		System.out.println();
		System.out.println("---- APARTAT 5.2 (comprovació) ----");
		i = comprovacioRendimentTmp.llistaArrayList.iterator();
		while (i.hasNext()) {
			Waypoint_Dades w = i.next();
			System.out.printf("El waypoint amb id = %d té el nom = %s\n", w.getId(), w.getNom());
		}

		return comprovacioRendimentTmp;
	}

	public static ComprovacioRendiment esborrarWaypoints(ComprovacioRendiment comprovacioRendimentTmp) {
//		System.out.println("---- APARTAT 1 ----");
//		for(Waypoint_Dades w : comprovacioRendimentTmp.llistaArrayList) {
//			if (w.getId() < 6) {
//				comprovacioRendimentTmp.llistaArrayList.remove(w);
//			}
//		}
//		No deixa esborrar l'element per l'excepcio de modificació de concurrencia. 
//		(https://docs.oracle.com/javase/7/docs/api/java/util/ConcurrentModificationException.html)

		System.out.println("---- APARTAT 2 (Iterator) ----");
		Iterator<Waypoint_Dades> iter = comprovacioRendimentTmp.llistaLinkedList.iterator();
		while (iter.hasNext()) {
			Waypoint_Dades w = iter.next();
			if (w.getId() > 4) {
				System.out.printf("Esborrat el waypoint amb id = %d\n", w.getId());
				iter.remove();
			}
		}

		System.out.println();
		System.out.println("---- APARTAT 2 (comprovació) ----");
		iter = comprovacioRendimentTmp.llistaLinkedList.iterator();
		while (iter.hasNext()) {
			Waypoint_Dades w = iter.next();
			System.out.printf("El waypoint amb id = %d té el nom = %s\n", w.getId(), w.getNom());
		}

		System.out.println();
		System.out.println("---- APARTAT 3 (listIterator) ----");
		ListIterator<Waypoint_Dades> lIter = comprovacioRendimentTmp.llistaLinkedList.listIterator();
		while (lIter.hasNext()) {
			Waypoint_Dades w = lIter.next();
			if (w.getId() == 2) {
				System.out.printf("Esborrat el waypoint amb id = %d\n", w.getId());
				lIter.remove();
			}
		}

		System.out.println();
		System.out.println("---- APARTAT 3 (comprovació) ----");
		while (lIter.hasPrevious()) {
			Waypoint_Dades w = lIter.previous();
			System.out.printf("El waypoint amb id = %d té el nom = %s\n", w.getId(), w.getNom());
		}
		return comprovacioRendimentTmp;
	}

	// !!!!!!! Modificar una copia del arraylist per no afectar a l'original
	public static ComprovacioRendiment modificarCoordenadesINomDeWaypoints(
			ComprovacioRendiment comprovacioRendimentTmp) {
		Scanner sc = new Scanner(System.in);
		for (Waypoint_Dades wp : comprovacioRendimentTmp.llistaArrayList) {
			if (wp.getId() % 2 == 0) {

				System.out.printf("----- Modificaciar el waypoint amb id = %d -----\n", wp.getId());

				// NOM del waypoint
				System.out.printf("Nom actual: %s\n", wp.getNom());
				System.out.printf("Nom nou: ");
				wp.setNom(sc.nextLine());

				System.out.println();

				String newCoordenates = "";
				// Checkers
				int[] arrayCoordenades = new int[3];
				int insertsCounter = 0;

				while (newCoordenates.split(" ").length != 3 || insertsCounter != 3) {
					insertsCounter = 0;
					// COORDENADES del waypoint
					// Pas 1 - Ask for coordinates
					System.out.printf("Coordenades actuals: (%d %d %d)\n", wp.getCoordenades()[0],
							wp.getCoordenades()[1], wp.getCoordenades()[2]);
					System.out.printf("Coordenades noves (format 1 17 7): ");
					newCoordenates = sc.nextLine();

					System.out.println();

					// Pas 2 - Secure 3 inputs with blank spaces.
					if (newCoordenates.split(" ").length != 3) {
						System.out.printf(
								"ERROR: introduir 3 paràmeters separats per 1 espai en blanc. Has introduït %d paràmetres\n",
								newCoordenates.split(" ").length);
					} else {
						// Pas 3 - Check that all 3 inputs are integers
						for (String c : newCoordenates.split(" ")) {
							if (Cadena.singIsInt(c)) {
								arrayCoordenades[insertsCounter] = Integer.parseInt(c);
								insertsCounter++;
							} else {
								System.out.printf("ERROR: coordenada %s no vàlida.\n", c);
							}
						}
					}
				}
				// Pas 4 - Set the coordinates
				wp.setCoordenades(arrayCoordenades);
			}
		}
		return comprovacioRendimentTmp;
	}

	// !!!!!!! Modificar una copia del arraylist per no afectar a l'original
	public static void visualitzarWaypointsOrdenats(ComprovacioRendiment comprovacioRendimentTmp) {
		Collections.sort(comprovacioRendimentTmp.llistaArrayList);
		for (Waypoint_Dades wp : comprovacioRendimentTmp.llistaArrayList) {
			System.out.println(wp);
		}
	}

	public static void waypointsACertaDistanciaMaxDeLaTerra(ComprovacioRendiment comprovacioRendimentTmp) {
		Scanner sc = new Scanner(System.in);
		String userDistanceString = "";
		while (!Cadena.singIsInt(userDistanceString)) {
			System.out.print("Distància màxima de la terra: ");
			userDistanceString = sc.nextLine();
		}
		int userDistance = Integer.parseInt(userDistanceString);
		for (Waypoint_Dades wp : comprovacioRendimentTmp.llistaArrayList) {
			if (Waypoint.distance(wp.getCoordenades()) <= userDistance) {
				System.out.println(wp);
			}
		}
	}

	public static String formatDate(LocalDateTime date) {
		if (date != null) {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
			return date.format(formatter);
		}
		return "NULL";
	}

	public static int distance(int[] coordinates) {
		int distance = 0;
		for (int co : coordinates) {
			distance += Math.pow(co, 2);
		}
		return distance;
	}

}
