/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daw.pauvidalroglan.krona;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author gmartinez
 */
public class Krona {
    /*
    SISTEMA DE NAVEGACIÓ BASAT EN WAYPOINTS.
    ES DONEN D'ALTA DIVERSOS WAYPOINTS DE L'ESPAI (ORBITA MARCIANA, PUNT LAGRANGE TERRA-LLUNA, PHOBOS, SATURN, LLUNA,...).
    ES PODEN MEMORITZAR DIVERSES RUTES AFEGINT DIVERSOS WAYPOINTS A CADA RUTA.
    
    */
	
	public static final int OBJECTES_A_CREAR = 10; // Per millorar la implementació a tots els metodes de Waypoint he decidit crear aquest atribut public.
    
    public static void bloquejarPantalla() {
        Scanner in = new Scanner(System.in);
        System.out.print("\nToca 'C' per a continuar ");
        while (in.hasNext()) {
            if ("C".equalsIgnoreCase(in.next())) break;
        }
    }
    
    
    public static void menuKrona() throws IOException  {
        String opcio;
        Scanner sc = new Scanner(System.in);
        StringBuilder menu = new StringBuilder("");
        
        ComprovacioRendiment comprovacioRendimentTmp = null;
        
        do {
            menu.delete(0, menu.length());
            
            menu.append(System.getProperty("line.separator"));
            menu.append("RV-18A Krona ");
            menu.append(System.getProperty("line.separator"));
            menu.append(System.getProperty("line.separator")); 
            
            menu.append("1. Inicialitzar el procés de comprovació d'un ArrayList i un LinkedList de waypoints");
            menu.append(System.getProperty("line.separator"));
            menu.append("2. Comprovar rendiment d'inicialització d'un ArrayList i un LinkedList de waypoints");
            menu.append(System.getProperty("line.separator"));
            menu.append("3. Comprovar rendiment d'inserció en un ArrayList i en un LinkedList de waypoints");
            menu.append(System.getProperty("line.separator"));
            menu.append("4. Modificació dels waypoints de l'ArrayList i del LinkedList");
            menu.append(System.getProperty("line.separator"));
            menu.append("5. Esborrar waypoints de l'ArrayList i del LinkedList.");
            menu.append(System.getProperty("line.separator"));
            menu.append("6. Modificar coordenades i nom dels waypoints de l'ArrayList amb ID parell.");
            menu.append(System.getProperty("line.separator"));
            menu.append("7. Visualitzar els waypoints de l'ArrayList ordenats.");
            menu.append(System.getProperty("line.separator"));
            menu.append("8. Llistar els waypoints de l'ArrayList que es trobin a certa distància de la Terra.");
            menu.append(System.getProperty("line.separator"));
            
            menu.append("10. Carregar a la BD les rutes");
            menu.append(System.getProperty("line.separator"));
            menu.append("11. Carregar a la BD les rutes carregades en memòria");
            menu.append(System.getProperty("line.separator"));
            menu.append("12. Llistar les rutes (només amb els ID dels waypoints)");
            menu.append(System.getProperty("line.separator"));
            menu.append("13. Llistar les rutes (amb tota la informació dels waypoints)");
            menu.append(System.getProperty("line.separator"));
            menu.append("14. Llistar els waypoints d'una ruta");
            menu.append(System.getProperty("line.separator"));
            menu.append("15. Crear un ruta");
            menu.append(System.getProperty("line.separator"));
            menu.append("16. Modificar una ruta");
            menu.append(System.getProperty("line.separator"));
            menu.append("17. Llistar les rutes que tinguin un waypoint concret");
            menu.append(System.getProperty("line.separator"));
            menu.append("18. Donar de baixar una ruta");
            menu.append(System.getProperty("line.separator"));
            menu.append("19. Esborrar de la BD una ruta");
            menu.append(System.getProperty("line.separator"));
            menu.append(System.getProperty("line.separator"));
            
            menu.append("20. Fer una copia de seguretat de la BD");
            menu.append(System.getProperty("line.separator"));
            menu.append("21. Esborrar tota la BD");
            menu.append(System.getProperty("line.separator"));
            menu.append(System.getProperty("line.separator"));
            
            menu.append("50. Tornar al menú pare (PNS-24 Puma)");
            menu.append(System.getProperty("line.separator"));
            
            menu.append("40. Inicialitzar un waypoint");
            menu.append(System.getProperty("line.separator"));
            menu.append("41. Insertar un waypoint");
            menu.append(System.getProperty("line.separator"));
            menu.append("42. Visualitzar els waypoints versus un tipus");
            menu.append(System.getProperty("line.separator"));
            menu.append("43. Visualitzar els nº de waypoints versus el seu tipus");
            menu.append(System.getProperty("line.separator"));
            menu.append("44. Trobar els waypoints versus el seu nom");
            menu.append(System.getProperty("line.separator"));
            menu.append("45. Ordenar els waypoints per data");
            menu.append(System.getProperty("line.separator"));
            
            System.out.print(MenuConstructorPantalla.constructorPantalla(menu));
            
            opcio = sc.next();
            
            
            switch (opcio) {
                case "1":
                	comprovacioRendimentTmp = Waypoint.inicialitzarComprovacioRendiment();
                    bloquejarPantalla();
                    break;
                case "2":
                	comprovacioRendimentTmp = Waypoint.comprovarRendimentInicialitzacio(OBJECTES_A_CREAR, comprovacioRendimentTmp);
                    bloquejarPantalla();
                    break;
                case "3":
                	comprovacioRendimentTmp = Waypoint.comprovarRendimentInsercio(comprovacioRendimentTmp);
                    bloquejarPantalla();
                    break;
                case "4":
                	comprovacioRendimentTmp = Waypoint.modificarWaypoints(comprovacioRendimentTmp);
                    bloquejarPantalla();
                    break;
                case "5":
                	comprovacioRendimentTmp = Waypoint.esborrarWaypoints(comprovacioRendimentTmp);
                    bloquejarPantalla();
                    break;
                case "6":
                	comprovacioRendimentTmp = Waypoint.modificarCoordenadesINomDeWaypoints(comprovacioRendimentTmp);
                	bloquejarPantalla();
                    break;
                case "7":
                    Waypoint.visualitzarWaypointsOrdenats(comprovacioRendimentTmp);
                    bloquejarPantalla();
                    break;
                case "8":
                	Waypoint.waypointsACertaDistanciaMaxDeLaTerra(comprovacioRendimentTmp);
                	bloquejarPantalla();
                	break;
                case "10":	
                    
                    bloquejarPantalla();
                    break;    
                case "12":
                    
                    bloquejarPantalla();
                    break;
                case "13":
                    
                    bloquejarPantalla();
                    break;
                case "15":
                    
                    bloquejarPantalla();
                    break;
                case "19":
                    
                    bloquejarPantalla();
                    break;
                    
                case "40":	
                    
                    bloquejarPantalla();
                    break;    
                case "41":
                    
                    bloquejarPantalla();
                    break;
                case "42":
                    
                    bloquejarPantalla();
                    break;
                case "43":
                    
                    bloquejarPantalla();
                    break;
                case "44":
                    
                    bloquejarPantalla();
                    break;
                case "45":
                	bloquejarPantalla();
                    break;
                    
                case "50":
                    break; 
                default:
                    System.out.println("COMANDA NO RECONEGUDA");
            }   
        } while (!opcio.equals("50"));
    }
    
}
