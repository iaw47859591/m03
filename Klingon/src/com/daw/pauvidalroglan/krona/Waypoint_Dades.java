package com.daw.pauvidalroglan.krona;

import java.text.CollationKey;
import java.text.Collator;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Locale;

public class Waypoint_Dades implements Comparable<Waypoint_Dades> {
	private int id;
	private String nom;
	private int[] coordenades;
	private boolean actiu;
	private LocalDateTime dataCreacio;
	private LocalDateTime dataAnulacio;
	private LocalDateTime dataModificacio;

	/**
	 * @param id
	 * @param nom
	 * @param coordenades
	 * @param actiu
	 * @param dataCreacio
	 * @param dataAnulacio
	 * @param dataModificacio
	 */
	protected Waypoint_Dades(int id, String nom, int[] coordenades, boolean actiu, LocalDateTime dataCreacio,
			LocalDateTime dataAnulacio, LocalDateTime dataModificacio) {
		this.id = id;
		this.nom = nom;
		this.coordenades = coordenades;
		this.actiu = actiu;
		this.dataCreacio = dataCreacio;
		this.dataAnulacio = dataAnulacio;
		this.dataModificacio = dataModificacio;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the coordenades
	 */
	public int[] getCoordenades() {
		return coordenades;
	}

	/**
	 * @param coordenades the coordenades to set
	 */
	public void setCoordenades(int[] coordenades) {
		this.coordenades = coordenades;
	}

	@Override
	public int compareTo(Waypoint_Dades wp) {
		int thisDistance = Waypoint.distance(this.coordenades);
		int wpDistance = Waypoint.distance(wp.coordenades);

		if (thisDistance > wpDistance) {
			return 1;
		} else if (thisDistance == wpDistance) {
			Collator c = Collator.getInstance(new Locale("es"));
			c.setStrength(Collator.TERTIARY);
			return c.compare(this.getNom(), wp.getNom());
		}
		return -1;
	}

	@Override
	public String toString() {
		String waypointStr = "WAYPOINT " + this.id + "\n\t" 
				+ "nom = " + this.nom + "\n\t" 
				+ "coordenades(x, y, z) = ("+ this.coordenades[0] + "," + this.coordenades[1] + "," + this.coordenades[2] 
				+ ") (distancia " + Waypoint.distance(this.coordenades) + ")\n\t" 
				+ "actiu = " + this.actiu + "\n\t" 
				+ "dataCreacio = "	+ Waypoint.formatDate(this.dataCreacio) + "\n\t" 
				+ "dataAnulacio= " + Waypoint.formatDate(this.dataAnulacio) + "\n\t"
				+ "dataModifiacio = " + Waypoint.formatDate(this.dataModificacio);
		return waypointStr;
	}

}
