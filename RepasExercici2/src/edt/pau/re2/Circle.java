package edt.pau.re2;

public class Circle {
	// Attributes declaration
	double radius;
	final double PI = 3.14; // Use a global variable to avoid calling Math.Pi()
	
	
	// Constructors
	
	/**
	 * Circle's constructor.
	 * 
	 * @param radius The circle's radius. 
	 */
	public Circle(double radius) {
		this.radius = radius;
	}
	
	// Methods
	
	/**
	 * Calculates the circle's area.
	 * 
	 * @return The circle's area.
	 */
	private double area() {
		// Calculate and returns the area -> {pi(3.14) * radius * radius}
		double area = (PI * this.radius * this.radius); 
		return area;
	}
	
	/**
	 * Calculates the circle's perimeter.
	 * 
	 * @return The circle's perimeter
	 */
	private double perimeter() {
		// Calculates and returns the perimeter -> {2 * pi * r}
		double perimeter = (2 * PI * this.radius); 
		return perimeter;
	}

	// Main
	public static void main(String[] args) {
		// Creates a Circle object (globe)
		Circle globe = new Circle(5);
		
		// Invoke methods
		double globeArea = globe.area();
		double globePerimeter = globe.perimeter();
		
		// Show results
		System.out.println("Area: " + globeArea);
		System.out.println("Perimeter: " + globePerimeter);
	}

}
