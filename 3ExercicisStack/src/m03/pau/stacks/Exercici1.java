package m03.pau.stacks;

import java.util.ArrayDeque;
import java.util.Deque;

public class Exercici1 {
	
	// Functions
	
	// No-generic
	/**
	 * Inverts an stack
	 * 
	 * @param stack - the stack to invert
	 * @return the inverted stack
	 */
	public static Deque<Integer> invertirPila(Deque<Integer> stack) {
		// Variables
		Deque<Integer> copyStackDeque = new ArrayDeque<Integer>(); // Copy to keep the original stack intact
		Deque<Integer> invertedStackDeque= new ArrayDeque<Integer>(); // Stack to keep the inverted stack elements
		// Copy the given stack
		for (Integer s : stack) {
			copyStackDeque.offer(s);
		}
		// Invert the deque
		while (!copyStackDeque.isEmpty()) {
			invertedStackDeque.push(copyStackDeque.pop());
		}
		return invertedStackDeque;
	}
	
	/**
	 * Print the elements from a Integer Deque
	 * 
	 * @param stack
	 */
	public static void showStack(Deque<Integer> stack) {
		// Loop printing elements
		for (Integer s : stack) {
			System.out.println(s);
		}
	}

	
	// Generic
	/**
	 * Inverts an stack
	 * 
	 * @param stack - the stack to invert
	 * @return the inverted stack
	 */
	public static <T> Deque<T> genericInvertirPila(Deque<T> stack) {
		// Variables
		Deque<T> copyStackDeque = new ArrayDeque<T>(); // Copy to keep the original stack intact
		Deque<T> invertedStackDeque= new ArrayDeque<T>(); // Stack to keep the inverted stack elements
		// Copy the given stack
		for (Object s : stack) {
			copyStackDeque.offer((T) s);
		}
		// Invert the deque
		while (!copyStackDeque.isEmpty()) {
			invertedStackDeque.push(copyStackDeque.pop());
		}
		return invertedStackDeque;
		
		
	}
	
	/**
	 * Print the elements from a Integer Deque
	 * 
	 * @param stack
	 */
	public static <T> void genericShowStack(Deque<T> stack) {
		// Loop printing elements
		for (Object s : stack) {
			System.out.println(s);
		}
		
	}

	public static void main(String[] args) {
		int[] integerElementsToAdd = {1,1,2,3,5,8,13,21};
		String[] stringElementsToAdd = {"Rick","Morty","Summer","Beth","Jerry"};
		
		// NO-GENERIC
		// Generate an integer stack
		Deque<Integer> integerStackDeque = new ArrayDeque<Integer>();
		// Add elements to the stack
		for (Integer i : integerElementsToAdd) {
			integerStackDeque.offer(i);
		}
		
		// Show the initial stack
		System.out.println("-- NO-GENERICS --");
		System.out.println("\nInitial stack: ");
		showStack(integerStackDeque);
		// Show the inverted stack
		System.out.println("\nInverted stack: ");
		showStack(invertirPila(integerStackDeque));
		
		// GENERIC
		// Generate an integer stack
		Deque<String> stringStackDeque = new ArrayDeque<String>();
		// Add elements to the stack
		for (String s : stringElementsToAdd) {
			stringStackDeque.offer(s);
		}
		
		System.out.println("-- GENERICS --");
		System.out.println("\nInteger stack: ");
		genericShowStack(integerStackDeque);
		System.out.println("\nString stack: ");
		genericShowStack(stringStackDeque);
		System.out.println("\nInverted Integer stack: ");
		genericShowStack(genericInvertirPila(integerStackDeque));
		System.out.println("\nInverted String stack: ");
		genericShowStack(genericInvertirPila(stringStackDeque));
		
		
		
	}
	
}
