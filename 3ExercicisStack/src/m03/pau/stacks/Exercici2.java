package m03.pau.stacks;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Scanner;

public class Exercici2 {

	public static String askUser() {
		// Scanner object to read keyboard input
		Scanner sc = new Scanner(System.in);
		// Ask the pile to the user
		System.out.println("Inserta l'input a checkejar: ");
		String d = sc.nextLine();
		return d;
	}

	public static int[] miningDiamonds(Deque<String> stack) {
		// Types of diamonds
		String initialWhiteDiamond = "<";
		String finalWhiteDiamond = ">";
		String initialBlackDiamond = "(";
		String finalBlackDiamond = ")";
		String currentItem = "";
		String previousItem = "";
		int whiteDiamondsCounter = 0;
		int blackDiamondsCounter = 0;
		int foundDiamondCounter = 0;
		// Make a copy of the given stack
		Deque<String> mineA = new ArrayDeque<String>();
		Deque<String> mineB = new ArrayDeque<String>();
		// Fill the A copy
		for (String d : stack) {
			mineA.offer(d);
		}
		do {
			// Restart the diamonds found counter
			foundDiamondCounter = 0;
			while (!mineA.isEmpty()) {
				// Save the current element
				currentItem = mineA.poll();				
				// Check for white diamond
				if (previousItem.equals(initialWhiteDiamond) && currentItem.equals(finalWhiteDiamond)) {
					// Remove diamond
					mineB.removeLast();
					// Add to the counters
					whiteDiamondsCounter++;
					foundDiamondCounter++;
				} else if (previousItem.equals(initialBlackDiamond) && currentItem.equals(finalBlackDiamond)) {
					// Remove diamond
					mineB.removeLast();
					// Add to the counters
					blackDiamondsCounter++;
					foundDiamondCounter++;
				} else {
					// Add the current element to b deque
					mineB.offer(currentItem);
					// Change current element to previous.
					previousItem = currentItem;
				}
			}
			// Change B to A and clear B
			while (!mineB.isEmpty()) {
				mineA.offer(mineB.poll());				
			}
			mineB.clear();
		} while (foundDiamondCounter != 0);
		// Create an array with the number of diamonds
		int[] arrayNumberWhiteBlackDiamonds = { whiteDiamondsCounter, blackDiamondsCounter };
		// Return the array with the number of diamonds
		return arrayNumberWhiteBlackDiamonds;
	}

	public static void printResult(int[] diamonds) {
		if (diamonds[0] == 0 && diamonds[1] == 0) {
			System.out.println("There are no diamonds in the mine");
		} else {
			System.out.printf("\nDiamonds\n\tWhite: %d\n\tBlack: %d", diamonds[0], diamonds[1]);
		}
	}

	public static void main(String[] args) {
		// Get the string with diamonds
		String diamants = askUser();
		// Generate a stack
		Deque<String> pilaDiamants = new ArrayDeque<String>();
		// Insert the diamonds into the stack
		for (String d : diamants.split("")) {
			pilaDiamants.offer(d);
		}
		
		// Count and remove diamonds
		int[] numberDiamonds = miningDiamonds(pilaDiamants);
		printResult(numberDiamonds);
	}
}
