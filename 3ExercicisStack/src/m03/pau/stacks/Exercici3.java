package m03.pau.stacks;

import java.util.ArrayDeque;
import java.util.Deque;

public class Exercici3 {

	public static void main(String[] args) {
		// Get the arithmetic expression
		String expression = Exercici2.askUser();
		String openParenthesis = "(";
		String closeParenthesis = ")";
		boolean errors = false;
		// Generate stacks
		Deque<String> cua = new ArrayDeque<String>();
		Deque<String> pila = new ArrayDeque<String>();
		// Insert the parenthesis into the stack
		for (String e : expression.split("")) {
			if (e.equals(openParenthesis) || e.equals(closeParenthesis)) {
				cua.offer(e);
			}
		}
		// Extract elements from the tail
		while (!cua.isEmpty()) {
			// If the element is an open parenthesis insert it into the pile
			if (cua.getFirst().equals(openParenthesis)) {
				pila.offer(cua.poll());
			} else {
				if (pila.isEmpty()) {
					errors = !errors;
					cua.removeFirst();
				} else {
					cua.removeFirst();
					pila.pollLast();
				}
			}
		}
		// Show the result
		if (!errors && pila.isEmpty()) {
			System.out.println("L'expressió es correcta.");
		} else {
			System.out.print("ERROR: ");
			if (!pila.isEmpty()) {
				System.out.println("L'expressió té algun parentesis obert de més");
			} else {
				System.out.println("L'expressió té algun parentesis tancat de més");
			}
		}
	}
}
