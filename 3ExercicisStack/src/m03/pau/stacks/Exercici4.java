package m03.pau.stacks;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;

public class Exercici4 {
	
	public static Deque<String> invertirPilaWithoutIterator(Deque<String> stack) {
		// Variables
		Deque<String> invertedStackDeque= new ArrayDeque<String>(); // Stack to keep the inverted stack elements
		// Invert the deque
		while (!stack.isEmpty()) {
			invertedStackDeque.push(stack.pop());
		}
		return invertedStackDeque;
	}
	
	public static Deque<String> invertirPilaWithIterator(Deque<String> stack) {
		// Variables
		Deque<String> invertedStackDeque= new ArrayDeque<String>(); // Stack to keep the inverted stack elements
		// Invert the deque
		Iterator<String> itr = stack.iterator();
		while (itr.hasNext()) {
			invertedStackDeque.addFirst((String) itr.next());
		}
		return invertedStackDeque;
	}
	
	public static void main(String[] args) {
		Deque<String> cua1 = new LinkedList<String>();
		Deque<String> cua2 = new ArrayDeque<>();

		cua1.addAll(Arrays.asList("1", "2", "3", "4"));
		cua2.addAll(Arrays.asList("1", "2", "3", "4"));
		
		System.out.println("Iterator - Cua 1");
		invertirPilaWithIterator(cua1);
		System.out.println("Iterator - Cua 2");
		invertirPilaWithIterator(cua2);
		System.out.println("No Iterator - Cua 1");
		invertirPilaWithoutIterator(cua1);
		System.out.println("No Iterator - Cua 2");
		invertirPilaWithoutIterator(cua2);

	}
}
