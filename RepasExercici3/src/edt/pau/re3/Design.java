package edt.pau.re3;

public class Design extends Employee{
	
	public double getSalary(int weeksWorked, double weeklyWage) {
		double salary = weeklyWage * weeksWorked;
		return salary;
	}

}
