package edt.pau.re3;

public class HR extends Employee{
	
	public double getSalary(int monthsWorked, double monthlyWage) {
		double salary = monthlyWage * monthsWorked;
		return salary;
	}

}
