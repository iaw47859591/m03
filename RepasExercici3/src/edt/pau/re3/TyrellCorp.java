package edt.pau.re3;

public class TyrellCorp {

	public static void main(String[] args) {
		// Creating objects
		Tech emp1 = new Tech();
		Design emp2 = new Design();
		HR emp3 = new HR();
		
		// Invoke set
		emp1.setData("N6MAA10816", "Roy Batty", 4);
		emp2.setData("N6FAB21416", "Pris Stratton", 4);
		emp3.setData("N6FAB61216", "Zhora Salome", 4);
		
		// Invoke getData and getSalary
		System.out.printf("EMPLOYEE 1: \n%s\nSalary: %.2f$\n\n", 
					emp1.getData(), emp1.getSalary(20, 3.5));
		System.out.printf("EMPLOYEE 2: \n%s\nSalary: %.2f$\n\n", 
				emp2.getData(), emp2.getSalary(20, 2.2));
		System.out.printf("EMPLOYEE 3: \n%s\nSalary: %.2f$\n\n", 
				emp3.getData(), emp3.getSalary(20, 3));
		
	}

}
