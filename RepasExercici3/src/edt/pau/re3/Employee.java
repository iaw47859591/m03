package edt.pau.re3;

public class Employee {
	String employeeID;
	String name;
	int age;
	
	public String getData() {
		String data = "ID: " + this.employeeID 
				+ "\nName: " + this.name 
				+ "\nAge: " + this.age;
		return data;
	}
	public void setData(String employeeID, String name, int age) {
		this.employeeID = employeeID;
		this.name = name;
		this.age = age;
	}
		
}
