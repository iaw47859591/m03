package edt.pau.re3;

public class Tech extends Employee {
	
	public double getSalary(int hoursWorked, double hourlyWage) {
		double salary = hourlyWage * hoursWorked;
		return salary;
	}
	
}
