package exc;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int user_id = sc.nextInt();

		try {
			if (user_id != 1234) {
				throw new InvalidUserIdException();
			} 
		} catch (Exception e) {
			System.out.println(e);
		}
		
		int current_balance = 1000;
		int deposit_amount = sc.nextInt();
		
		try {
			if (deposit_amount < 0) {
				throw new NegativeNotAllowedException();
			} else {
				current_balance += deposit_amount;
				System.out.println(current_balance);
			}
		} catch (Exception e) {
			
		}
	}
}
