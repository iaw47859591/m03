package com.daw.pau.exc;

public class Exercici1part1 {

	public static void exercici1Apartats1i2() {

		System.out.println("Comença exercici 1.1");
		// Exercici 1.1
		// Declare an array with 3 positions
		// int[] array = new int[3];

		// trying to read a fourth position will trhow an error
		// int wrongPosition = array[4];

		// SORTIDA
		// Exception in thread "main" java.lang.ArrayIndexOutOfBoundsException: Index 4
		// out of bounds for length 3
		// at
		// com.daw.pau.exc.Exercici1part1.exercici1Apartats1i2(Exercici1part1.java:11)
		// at com.daw.pau.exc.GestioErrors.main(GestioErrors.java:31)

		// Exercici 1.2
		// System.out.println("Final del programa!");
		// RESPOSTA:
		// No s'executarà ja que el error atura l'execució del programa

		// Exercici 1.3
		// RESPOSTA:
		// Podriem mostrar l'anterior print amb un try - catch - finally.
		// Afegint aquest dintre el finally.
		// O directament fora del try catch.
		// Ja que al usar el try-catch l'excepció no atura l'execució del programa.

		// Exercici 1.4
		// Declare an array with 3 positions
		int[] array = new int[3];

		// trying to read a fourth position will throw an error
		try {
			int wrongPosition = array[4];
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Codi del catch");
		} finally {
			// Exercici 1.5
			System.out.println("Codi del finally");
		}

		System.out.println("Final del programa!");

		// RESPOSTA: Es mostra ja que al usar el try - catch no aturem l'execució del
		// programa

		
		System.out.println("\nComença exercici 1.6");
		// Exercici 1.6 i 1.7
		// Declare an array with 3 positions
//		int[] array2 = new int[3];
//
//		// trying to read a fourth position will throw an error
//		try {
//			int wrongPosition = array2[4];
//		} catch (StringIndexOutOfBoundsException e) { 	
//			System.out.println("Codi del catch");
//		}

		// SORTIDA
//		Exception in thread "main" java.lang.ArrayIndexOutOfBoundsException: Index 4 out of bounds for length 3
//		at com.daw.pau.exc.Exercici1part1.exercici1Apartats1i2(Exercici1part1.java:58)
//		at com.daw.pau.exc.GestioErrors.main(GestioErrors.java:31)

		// RESPOSTA: Using StringIndexOutOfBoundsExceptions genere un error ja que no
		// correspon a l'excepció que es realitza

		// Declare an array with 3 positions
		int[] array2 = new int[3];

		// trying to read a fourth position will throw an error
		try {
			int wrongPosition = array2[4];
		} catch (Exception e) {
			System.out.println("Codi del catch");
			System.out.println("RESPOSTA 1.7");
			System.out.println("getMEssage() : " + e.getMessage());
			System.out.println("getCause() : " + e.getCause());
			System.out.print("printStackTrace()");
			e.printStackTrace();
		}

		// RESPOSTA 1.6: No genera cap error perque la classe Exception conté (esta a un nivell superior) el ArrayIndexOutOfBoundException.
		
		
		System.out.println("\nComença exercici 1.8");
		// Exercici 1.8
		// Declare an array with 3 positions
				int[] array3 = new int[3];

				// trying to read a fourth position will throw an error
				try {
					int wrongPosition = array3[4];
				} catch (Exception e) {
					System.out.println("Codi del catch");
					System.out.println("RESPOSTA 1.7");
					System.out.println("getMEssage() : " + e.getMessage());
					System.out.println("getCause() : " + e.getCause());
					System.out.print("printStackTrace()");
					e.printStackTrace();
				}

	}

}
