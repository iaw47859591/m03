package com.daw.eva.ecom;


import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.daw.eva.ecom.business.entities.Producte;

public class ProductJDBCDAO implements ProductDAO{

	// Conexión a la base de datos
    private static Connection conn = null;

    // Configuración de la conexión a la base de datos
    private static final String DB_HOST = "localhost";
    private static final String DB_PORT = "3307";
    private static final String DB_NAME = "tienda";
    private static final String DB_URL = "jdbc:mysql://" + DB_HOST + ":" + DB_PORT + "/" + DB_NAME + "?serverTimezone=UTC";
    private static final String DB_USER = "root";
    private static final String DB_PASS = "password";
    private static final String DB_MSQ_CONN_OK = "CONEXIÓN CORRECTA";
    private static final String DB_MSQ_CONN_NO = "ERROR EN LA CONEXIÓN";

    // Configuración de la tabla Clientes
    private static final String DB_CLI = "productos";
    private static final String DB_CLI_SELECT = "SELECT * FROM " + DB_CLI;
    private static final String DB_CLI_ID = "id";
    private static final String DB_CLI_NOM = "nombre";
    private static final String DB_CLI_DESC = "descripcion";
    private static final String DB_CLI_PREC = "precio";
    private static Connection connection = null;

    //////////////////////////////////////////////////
    // MÉTODOS DE CONEXIÓN A LA BASE DE DATOS
    //////////////////////////////////////////////////
    ;
    
    /**
     * Intenta cargar el JDBC driver.
     * @return true si pudo cargar el driver, false en caso contrario
     */
    public static boolean loadDriver() {
        try {
            System.out.print("Loading Driver...");
            Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
            System.out.println("OK!");
            return true;
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            return false;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    /**
     * Intenta conectar con la base de datos.
     *
     * @return true si pudo conectarse, false en caso contrario
     */
    public static boolean connect() {
        try {
        	conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
        	System.out.println("Base de dades conectada");
        	return true;
        } catch (SQLException e) {
        	throw new IllegalStateException("No s'ha pogut connectar a la base de dades", e);
        }
    }

    /**
     * Comprueba la conexión y muestra su estado por pantalla
     *
     * @return true si la conexión existe y es válida, false en caso contrario
     * @throws SQLException 
     */
    public static boolean isConnected() throws SQLException {
        // Comprobamos estado de la conexión
    	try {
    		if (conn.isClosed()) {
    			System.out.println(DB_MSQ_CONN_NO);
            	return false;
            } else {
            	System.out.println(DB_MSQ_CONN_OK);
            	return true;
            }
    	} catch (SQLException ex) {
    		System.out.println(DB_MSQ_CONN_NO);
    		return false;
    	}
    }

    /**
     * Cierra la conexión con la base de datos
     * @throws SQLException 
     */
    public static void close() throws SQLException {
    	try {
    		System.out.println("CLOSING connection");
    		conn.close();
    		System.out.println("Connection CLOSED");
    	} catch (SQLException ex) {
    		ex.printStackTrace();
    		System.out.println(DB_MSQ_CONN_NO);
    	}
    }
	
    public static ResultSet getTablaProducts(int resultSetType, int resultSetConcurrency) throws DAOException {
        try {
        	// Fem la conexio a la base de dades
        	Connection conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
        	CallableStatement sentSQL = conn.prepareCall("CALL getProducts()", resultSetType, resultSetConcurrency);
        	ResultSet rs = sentSQL.executeQuery();
        	return rs;
        } catch (SQLException ex) {
        	throw new DAOException(ex);
        }

    }
    
	public void printTablaProductos() throws DAOException {
		try {
    		// Obtenim el resultat
    		ResultSet res = ProductJDBCDAO.getTablaProducts(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
    		// Recorrem el resultat
    		while (res.next()) {
    		    // Agafem els camps
    			int id = res.getInt(DB_CLI_ID);
    			String name = res.getString(DB_CLI_NOM);
    			String des = res.getString(DB_CLI_DESC);
    			double price = res.getFloat(DB_CLI_PREC);
    			System.out.println("PRODUCT\n\tID: " + id + "\n\tName: " + name + "\n\tDesc: " + des + "\n\tPrice: " + price);
    		}
    	} catch (SQLException ex) {
    		throw new DAOException(ex);
    	}
		
	}

	
	public ResultSet getProductById(int id) throws DAOException {
		try {
        	// Fem la conexio a la base de dades
        	Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
        	CallableStatement stm = connection.prepareCall("CALL getProductById(?)", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
        	stm.setLong(1, id);
        	ResultSet res = stm.executeQuery();
        	res.next();
        	return res;
        } catch(Exception ex) {
        	throw new DAOException(ex);
        }
	}

}