package com.daw.eva.ecom;

import java.sql.SQLException;
import java.util.List;

import com.daw.eva.ecom.DAOException;
import com.daw.eva.ecom.business.entities.Client;

public interface ClientDAO {

	//TO-DO crea el contracte o llista de mètodes a oferir
	void printTablaClientes() throws DAOException, SQLException;
	boolean existsCliente(int id) throws DAOException;
	boolean insertCliente(String nom, String direccio) throws DAOException;
	boolean updateCliente(int id, String nom, String direccio) throws DAOException;
	boolean deleteCliente(int id) throws DAOException;

	
//	Client getColorById(long id) throws DAOException;
//    List<Color> getColors() throws DAOException;
//    List<Color> getColors(int offset, int count) throws DAOException;
//    List<Color> getColors(String searchTerm) throws DAOException;
//    List<Color> getColors(String searchTerm, int offset, int count) throws DAOException;
    
}
