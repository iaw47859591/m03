package com.daw.pauvidalroglan.krona;

import java.time.LocalDateTime;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.TreeSet;

import Varies.Data;

public class Ruta {

	public static Boolean checkUserErrorsInputRoutes(String route, ComprovacioRendiment comprovacioRendimentTmp) {
		int lenghtAdmitted = 2;
		int correctRoute = 0;
		if (route.split(" ").length != lenghtAdmitted) {
			System.out.printf(
					"ERROR: introduir %d parametres separats per 1 espai en blanc. Has introduit %d parametres.\n",
					lenghtAdmitted, route.split(" ").length);
			return true;
		} else {
			for (String id : route.split(" ")) {
				if (!isInteger(id)) {
					System.out.printf("ERROR: has introduit %s com a ruta. Els ID de les rutes són integers.\n", id);
					return true;
				}
			}
			HashSet<Integer> idHash = new HashSet<Integer>();
			for (Ruta_Dades r : comprovacioRendimentTmp.llistaRutes) {
				idHash.add(r.getId());
			}
			for (String id : route.split(" ")) {
				if (!idHash.contains(Integer.parseInt(id))) {
					System.out.printf("ERROR: no existeix la ruta %s en el sistema.\n", id);
					return true;
				}
			}
		}
		return false;
	}

	public static boolean isInteger(String s) {
		try {
			Integer.parseInt(s);
		} catch (NumberFormatException e) {
			return false;
		} catch (NullPointerException e) {
			return false;
		}
		// only got here if we didn't return false
		return true;
	}

	public static List<Waypoint_Dades> crearRutaInicial() {

		List<Waypoint_Dades> llistaWaypointLinkedList = null;

		llistaWaypointLinkedList = new LinkedList<Waypoint_Dades>();

		llistaWaypointLinkedList.add(new Waypoint_Dades(0, "Òrbita de la Terra", new int[] { 0, 0, 0 }, true,
				LocalDateTime.parse("21-10-2020 01:10", Data.formatter), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter)));
		llistaWaypointLinkedList.add(new Waypoint_Dades(1, "Punt Lagrange Terra-LLuna", new int[] { 1, 1, 1 }, true,
				LocalDateTime.parse("21-10-2020 01:00", Data.formatter), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter)));
		llistaWaypointLinkedList.add(new Waypoint_Dades(2, "Òrbita de la LLuna", new int[] { 2, 2, 2 }, true,
				LocalDateTime.parse("21-10-2020 00:50", Data.formatter), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter)));
		llistaWaypointLinkedList.add(new Waypoint_Dades(3, "Òrbita de Mart", new int[] { 3, 3, 3 }, true,
				LocalDateTime.parse("21-10-2020 00:40", Data.formatter), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter)));
		llistaWaypointLinkedList.add(new Waypoint_Dades(4, "Òrbita de Júpiter", new int[] { 4, 4, 4 }, true,
				LocalDateTime.parse("21-10-2020 00:30", Data.formatter), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter)));
		llistaWaypointLinkedList.add(new Waypoint_Dades(5, "Punt Lagrange Júpiter-Europa", new int[] { 5, 5, 5 }, true,
				LocalDateTime.parse("21-10-2020 00:20", Data.formatter), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter)));
		llistaWaypointLinkedList.add(new Waypoint_Dades(6, "Òrbita de Europa", new int[] { 6, 6, 6 }, true,
				LocalDateTime.parse("21-10-2020 00:10", Data.formatter), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter)));
		llistaWaypointLinkedList.add(new Waypoint_Dades(7, "Òrbita de Venus", new int[] { 7, 7, 7 }, true,
				LocalDateTime.parse("21-10-2020 00:01", Data.formatter), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter)));

		return llistaWaypointLinkedList;
	}

	public static ComprovacioRendiment inicialitzarRuta(ComprovacioRendiment comprovacioRendimentTmp) {
		List<Waypoint_Dades> llistaWaypoints = crearRutaInicial();

		for (Waypoint_Dades w : llistaWaypoints) {
			comprovacioRendimentTmp.pilaWaypoints.addFirst(w);
		}

		Waypoint_Dades newWaypoint = new Waypoint_Dades(4, "Òrbita de Júpiter REPETIDA", new int[] { 4, 4, 4 }, true,
				LocalDateTime.parse("21-10-2020 00:30", Data.formatter), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter));

		comprovacioRendimentTmp.pilaWaypoints.addFirst(newWaypoint);

		comprovacioRendimentTmp.setWtmp(newWaypoint);

		return comprovacioRendimentTmp;

	}

	public static void visualitzarRuta(ComprovacioRendiment comprovacioRendimentTmp) {
		for (Waypoint_Dades w : comprovacioRendimentTmp.pilaWaypoints) {
			System.out.println(w);
		}
	}

	public static void invertirRuta(ComprovacioRendiment comprovacioRendimentTmp) {
		Deque<Waypoint_Dades> pilaWaypointsInversa = new ArrayDeque<Waypoint_Dades>();

		for (Waypoint_Dades w : comprovacioRendimentTmp.pilaWaypoints) {
			pilaWaypointsInversa.addFirst(w);
		}

		for (Waypoint_Dades w : pilaWaypointsInversa) {
			System.out.println(w);
		}
	}

	public static void existeixWaypointEnRuta(ComprovacioRendiment comprovacioRendimentTmp) {
		if (comprovacioRendimentTmp.pilaWaypoints.contains(comprovacioRendimentTmp.wtmp)) {
			System.out.printf("SI hem trobat el waypoint %s emmagatzemat en comprovacióRendimentTmp, a la llista.\n",
					comprovacioRendimentTmp.wtmp.getNom());
		} else {
			System.out.printf("NO hem trobat el waypoint %s emmagatzemat en comprovacióRendimentTmp, a la llista.\n",
					comprovacioRendimentTmp.wtmp.getNom());
		}

		Waypoint_Dades newWaipont = new Waypoint_Dades(4, "Òrbita de Júpiter REPETIDA", new int[] { 4, 4, 4 }, true,
				LocalDateTime.parse("21-10-2020 00:30", Data.formatter), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter));

		if (comprovacioRendimentTmp.pilaWaypoints.contains(newWaipont)) {
			System.out.printf("SI hem trobat el waypoint %s creat ara mateix, a la llista.\n",
					comprovacioRendimentTmp.wtmp.getNom());
		} else {
			System.out.printf("NO hem trobat el waypoint %s creat ara mateix, a la llista.\n",
					comprovacioRendimentTmp.wtmp.getNom());
		}
	}

	public static ComprovacioRendiment inicialitzaLListaRutes(ComprovacioRendiment comprovacioRendimentTmp) {
		// Generating routes
		Ruta_Dades ruta_0 = new Ruta_Dades(0, "ruta 0: Terra --> Punt Lagrange Júpiter-Europa",
				new ArrayList<Integer>(Arrays.asList(0, 1, 2, 3, 4, 5)), true,
				LocalDateTime.parse("28-10-2020 16:30", Data.formatter), null,
				LocalDateTime.parse("28-10-2020 16:30", Data.formatter));
		Ruta_Dades ruta_1 = new Ruta_Dades(1, "ruta 1: Terra --> Òrbita de Mart (directe)",
				new ArrayList<Integer>(Arrays.asList(0, 3)), true,
				LocalDateTime.parse("28-10-2020 16:31", Data.formatter), null,
				LocalDateTime.parse("28-10-2020 16:31", Data.formatter));
		Ruta_Dades ruta_2 = new Ruta_Dades(2, "ruta 2.1: Terra --> Òrbita de Venus",
				new ArrayList<Integer>(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7)), true,
				LocalDateTime.parse("28-10-2020 16:32", Data.formatter), null,
				LocalDateTime.parse("28-10-2020 16:32", Data.formatter));
		Ruta_Dades ruta_3 = new Ruta_Dades(3, "ruta 3: Terra --> Mart (directe) --> Òrbita de Júpiter",
				new ArrayList<Integer>(Arrays.asList(0, 3, 4)), true,
				LocalDateTime.parse("28-10-2020 16:33", Data.formatter), null,
				LocalDateTime.parse("28-10-2020 16:33", Data.formatter));
		Ruta_Dades ruta_4 = new Ruta_Dades(4, "ruta 2.2: Terra --> Òrbita de Venus (REPETIDA)",
				new ArrayList<Integer>(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7)), true,
				LocalDateTime.parse("28-10-2020 16:32", Data.formatter), null,
				LocalDateTime.parse("30-10-2020 19:49", Data.formatter));

		// Adding routes
		comprovacioRendimentTmp.llistaRutes.add(ruta_0);
		comprovacioRendimentTmp.llistaRutes.add(ruta_1);
		comprovacioRendimentTmp.llistaRutes.add(ruta_2);
		comprovacioRendimentTmp.llistaRutes.add(ruta_3);
		comprovacioRendimentTmp.llistaRutes.add(ruta_4);

		System.out.println("Insertades les rutes: ");
		for (Ruta_Dades r : comprovacioRendimentTmp.llistaRutes) {
			System.out.println(r);
		}

		return comprovacioRendimentTmp;
	}

	public static void setUnio(ComprovacioRendiment comprovacioRendimentTmp) {
		HashSet<Integer> waypoints = new HashSet<Integer>();
		for (Ruta_Dades r : comprovacioRendimentTmp.llistaRutes) {
			for (Integer ID : r.getWaypoints()) {
				waypoints.add(ID);
			}
		}
		System.out.printf("ID dels waypoints ficats en el set: %s", waypoints);
	}

	public static void setInterseccio(ComprovacioRendiment comprovacioRendimentTmp) {
		HashSet<Integer> waypoints = new HashSet<Integer>();

		for (Ruta_Dades r : comprovacioRendimentTmp.llistaRutes) {
			for (Integer ID : r.getWaypoints()) {
				waypoints.add(ID);
			}
		}

		Iterator<Integer> itr = waypoints.iterator();
		while (itr.hasNext()) {
			int idInHash = itr.next();
			for (Ruta_Dades r : comprovacioRendimentTmp.llistaRutes) {
				if (!r.getWaypoints().contains(idInHash)) {
					itr.remove();
					break;
				}
			}
		}
		System.out.printf("ID dels waypoints en totes les rutes: %s", waypoints);
	}

	private static int buscarRuta(int numRuta, ComprovacioRendiment comprovacioRendimentTmp) {
		int posicioRuta = 0;

		for (Ruta_Dades r : comprovacioRendimentTmp.llistaRutes) {
			if (r.getId() == numRuta) {
				return posicioRuta;
			}
			posicioRuta++;
		}
		return -1;
	}

	public static void setResta(ComprovacioRendiment comprovacioRendimentTmp) {
		Scanner sc = new Scanner(System.in);
		String userRoute = "";
		System.out.println("Rutes actuals: ");
		for (Ruta_Dades r : comprovacioRendimentTmp.llistaRutes) {
			System.out.printf("ID %d: %s\n", r.getId(), r);
		}

		do {
			System.out.print("\nSelecciona ruta A i B (format: 3 17): ");
			userRoute = sc.nextLine();
		} while (checkUserErrorsInputRoutes(userRoute, comprovacioRendimentTmp));

		int rutaA = buscarRuta(Integer.parseInt(userRoute.split(" ")[0]), comprovacioRendimentTmp);
		int rutaB = buscarRuta(Integer.parseInt(userRoute.split(" ")[1]), comprovacioRendimentTmp);

		HashSet<Integer> waypoints = new HashSet<Integer>();

		for (Integer w : comprovacioRendimentTmp.llistaRutes.get(rutaA).getWaypoints()) {
			waypoints.add(w);
		}
		System.out.println("HashSet (havent-hi afegit els waypoints de la ruta A) = " + waypoints);

		for (Integer w : comprovacioRendimentTmp.llistaRutes.get(rutaB).getWaypoints()) {
			if (waypoints.contains(w)) {
				waypoints.remove(w);
			}
		}
		System.out.println("HashSet (havent-hi tret els waypoints de la ruta B) = " + waypoints);
	}

	public static void crearSetOrdenatDeRutes(ComprovacioRendiment comprovacioRendimentTmp) {
		TreeSet<Ruta_Dades> waypoints = new TreeSet<Ruta_Dades>(new Comparator<Ruta_Dades>() {
			@Override
			public int compare(Ruta_Dades r1, Ruta_Dades r2) {
				if (r1.getWaypoints().equals(r2.getWaypoints())) {
					return 0;
				} else {
					if (r1.getId() < r2.getId()) {
						return 1;
					}
				}
				return -1;
			}
		});

		for (Ruta_Dades r : comprovacioRendimentTmp.llistaRutes) {
			waypoints.add(r);
		}

		Iterator<Ruta_Dades> itr = waypoints.iterator();
		while (itr.hasNext()) {
			Ruta_Dades r = itr.next();
			System.out.printf("ID %d: %s\n", r.getId(), r);
		}
	}

	public static void crearLinkedHashMapDeRutes(ComprovacioRendiment comprovacioRendimentTmp) {
		for (Ruta_Dades r : comprovacioRendimentTmp.llistaRutes) {
			if (!comprovacioRendimentTmp.mapaLinkedDeRutes.containsKey(r.getId())) {
				comprovacioRendimentTmp.mapaLinkedDeRutes.put(r.getId(), r);
			}
		}
		
		
		
	}

}