package com.daw.pauvidalroglan.krona;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;

public class Ruta_Dades {
	private int id;
	private String nom;
	private ArrayList<Integer> waypoints;
	private boolean actiu;
	private LocalDateTime dataCreacio;
	private LocalDateTime dataAnulacio;
	private LocalDateTime dataModificacio;

	/**
	 * @param id
	 * @param nom
	 * @param waypoints
	 * @param actiu
	 * @param dataCreacio
	 * @param dataAnulacio
	 * @param dataModificacio
	 */
	public Ruta_Dades(int id, String nom, ArrayList<Integer> waypoints, boolean actiu, LocalDateTime dataCreacio,
			LocalDateTime dataAnulacio, LocalDateTime dataModificacio) {
		this.id = id;
		this.nom = nom;
		this.waypoints = waypoints;
		this.actiu = actiu;
		this.dataCreacio = dataCreacio;
		this.dataAnulacio = dataAnulacio;
		this.dataModificacio = dataModificacio;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the waypoints
	 */
	public ArrayList<Integer> getWaypoints() {
		return waypoints;
	}

	/**
	 * @param waypoints the waypoints to set
	 */
	public void setWaypoints(ArrayList<Integer> waypoints) {
		this.waypoints = waypoints;
	}

	@Override
	public String toString() {
//		String[] waypointsArray = waypoints.toArray(new String[waypoints.size()]);
//		String waypointsString = Arrays.toString(waypointsArray);
		String ruta = nom + ": waypoints" + waypoints;
		return ruta;
	}

}
