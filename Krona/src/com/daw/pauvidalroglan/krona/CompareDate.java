package com.daw.pauvidalroglan.krona;

import java.time.LocalDateTime;
import java.util.Comparator;

public class CompareDate implements Comparator<Waypoint_Dades>{

	@Override
	public int compare(Waypoint_Dades wp1, Waypoint_Dades wp2) {
		return wp1.getDataCreacio().compareTo(wp2.getDataCreacio());
	}

}
