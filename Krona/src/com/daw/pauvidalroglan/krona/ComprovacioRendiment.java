package com.daw.pauvidalroglan.krona;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

public class ComprovacioRendiment {
	// LIST
	int[] coordenadesTmp = null;
	ArrayList<Waypoint_Dades> llistaArrayList;		
	LinkedList<Waypoint_Dades> llistaLinkedList;	
	// STACK
	Waypoint_Dades wtmp;
	public Deque<Waypoint_Dades> pilaWaypoints;
	// SET
	public ArrayList<Ruta_Dades> llistaRutes;
	// MAP
	public LinkedHashMap<Integer, Ruta_Dades> mapaLinkedDeRutes;
	// EXERCICI 2 -- EXAMEN
	// Inici
	LinkedList<Waypoint_Dades> llistaWaypoints = new LinkedList<Waypoint_Dades>();
	LinkedHashMap<Integer, Waypoint_Dades> mapaWaypoints;
	// Fi
	
	public ComprovacioRendiment() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		
		this.coordenadesTmp = new int[] {0,0,0};
		this.llistaArrayList = new ArrayList<Waypoint_Dades>();		//
		this.llistaLinkedList = new LinkedList<Waypoint_Dades>();
		this.wtmp = new Waypoint_Dades();
		this.pilaWaypoints = new ArrayDeque<Waypoint_Dades>();
		this.llistaRutes = new ArrayList<Ruta_Dades>();
		this.mapaLinkedDeRutes = new LinkedHashMap<Integer, Ruta_Dades>();
		//this.waypoint = new Waypoint_Dades(0, "Òrbita de la Terra", coordenadesTmp, true, LocalDateTime.parse("15-08-1954 00:01", formatter), null, null);
	}
	
	/**
	 * @return the wtmp
	 */
	public Waypoint_Dades getWtmp() {
		return wtmp;
	}

	/**
	 * @param wtmp the wtmp to set
	 */
	public void setWtmp(Waypoint_Dades wtmp) {
		this.wtmp = wtmp;
	}

	
}
