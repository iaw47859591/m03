package com.daw.pauvidalroglan.krona;

import java.text.Collator;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

import Llibreries.Cadena;
import Varies.Data;

public class Waypoint {

	private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

	public static ComprovacioRendiment inicialitzarComprovacioRendiment() {
		ComprovacioRendiment comprovacioRendimentTmp = new ComprovacioRendiment();
		return comprovacioRendimentTmp;
	}

	public static ComprovacioRendiment comprovarRendimentInicialitzacio(int numObjACrear,
			ComprovacioRendiment comprovacioRendimentTmp) {
		// Variables declaration
		long starterTime;
		long endTime;

		// Date formatter pattern

		// ARRAY LIST
		// Get the starting nanoseconds
		starterTime = System.nanoTime();
		for (int i = 0; i < numObjACrear; i++) {
			comprovacioRendimentTmp.llistaArrayList
					.add(new Waypoint_Dades(0, "Òrbita de la Terra", comprovacioRendimentTmp.coordenadesTmp, true,
							LocalDateTime.parse("15-08-1954 00:01", formatter), null, null, 0));
		}
		// Get the ending nanoseconds
		endTime = System.nanoTime();
		// Print the time in milisecons
		System.out.println("Temps per insertar " + numObjACrear + " waypoints a l'ArrayList: "
				+ Duration.ofNanos(endTime - starterTime).toMillis());

		// LINKED LIST
		// Get the starting nanoseconds
		starterTime = System.nanoTime();
		for (int i = 0; i < numObjACrear; i++) {
			comprovacioRendimentTmp.llistaLinkedList
					.add(new Waypoint_Dades(1, "Òrbita de la Terra", comprovacioRendimentTmp.coordenadesTmp, true,
							LocalDateTime.parse("15-08-1954 00:01", formatter), null, null, 6));
		}
		// Get the ending nanoseconds
		endTime = System.nanoTime();
		// Print the time in milisecons
		System.out.println("Temps per insertar " + numObjACrear + " waypoints a l'ArrayList: "
				+ Duration.ofNanos(endTime - starterTime).toMillis());
		return comprovacioRendimentTmp;
	}

	public static ComprovacioRendiment comprovarRendimentInsercio(ComprovacioRendiment comprovacioRendimentTmp) {
		int centerList = Krona.OBJECTES_A_CREAR / 2;
		long starterTime;
		long endTime;

		// Add component FIRST position
		// Array
		starterTime = System.nanoTime();
		comprovacioRendimentTmp.llistaArrayList.add(0,
				new Waypoint_Dades(0, "Òrbita de la Terra", comprovacioRendimentTmp.coordenadesTmp, true,
						LocalDateTime.parse("15-08-1954 00:01", formatter), null, null));

		// Get the ending nanoseconds
		endTime = System.nanoTime();
		// Print the time in milisecons
		System.out.println("Temps per insertar 1 waypoint a la 1a positicó de l'ArrayList: "
				+ Duration.ofNanos(endTime - starterTime).getNano());

		// Linked
		starterTime = System.nanoTime();
		comprovacioRendimentTmp.llistaLinkedList
				.addFirst(new Waypoint_Dades(0, "Òrbita de la Terra", comprovacioRendimentTmp.coordenadesTmp, true,
						LocalDateTime.parse("15-08-1954 00:01", formatter), null, null));

		// Get the ending nanoseconds
		endTime = System.nanoTime();
		// Print the time in milisecons
		System.out.println("Temps per insertar 1 waypoint a la 1a positicó del LinkedList: "
				+ Duration.ofNanos(endTime - starterTime).getNano());

		// Add component MIDDLE position
		// Array
		starterTime = System.nanoTime();
		comprovacioRendimentTmp.llistaArrayList.add(centerList,
				new Waypoint_Dades(0, "Òrbita de la Terra", comprovacioRendimentTmp.coordenadesTmp, true,
						LocalDateTime.parse("15-08-1954 00:01", formatter), null, null));

		// Get the ending nanoseconds
		endTime = System.nanoTime();
		// Print the time in milisecons
		System.out.println("Temps per insertar 1 waypoint al mig (pos. " + centerList + ") positicó de l'ArrayList: "
				+ Duration.ofNanos(endTime - starterTime).getNano());

		// Linked
		starterTime = System.nanoTime();
		comprovacioRendimentTmp.llistaLinkedList.add(centerList,
				new Waypoint_Dades(0, "Òrbita de la Terra", comprovacioRendimentTmp.coordenadesTmp, true,
						LocalDateTime.parse("15-08-1954 00:01", formatter), null, null));

		// Get the ending nanoseconds
		endTime = System.nanoTime();
		// Print the time in milisecons
		System.out.println("Temps per insertar 1 waypoint al mig (pos. " + centerList + ") positicó del LinkedList: "
				+ Duration.ofNanos(endTime - starterTime).getNano());

		// Add component LAST position
		// Array
		starterTime = System.nanoTime();
		comprovacioRendimentTmp.llistaArrayList.add(comprovacioRendimentTmp.llistaArrayList.size(),
				new Waypoint_Dades(0, "Òrbita de la Terra", comprovacioRendimentTmp.coordenadesTmp, true,
						LocalDateTime.parse("15-08-1954 00:01", formatter), null, null));

		// Get the ending nanoseconds
		endTime = System.nanoTime();
		// Print the time in milisecons
		System.out.println("Temps per insertar 1 waypoint al final de l'ArrayList: "
				+ Duration.ofNanos(endTime - starterTime).getNano());
		// Linked
		starterTime = System.nanoTime();
		comprovacioRendimentTmp.llistaLinkedList
				.addLast(new Waypoint_Dades(0, "Òrbita de la Terra", comprovacioRendimentTmp.coordenadesTmp, true,
						LocalDateTime.parse("15-08-1954 00:01", formatter), null, null));

		// Get the ending nanoseconds
		endTime = System.nanoTime();
		// Print the time in milisecons
		System.out.println("Temps per insertar 1 waypoint al  al final del LinkedList: "
				+ Duration.ofNanos(endTime - starterTime).getNano());

		return comprovacioRendimentTmp;
	}

	public static ComprovacioRendiment modificarWaypoints(ComprovacioRendiment comprovacioRendimentTmp) {
		System.out.println("---- APARTAT 1 ----");
		List<Integer> idsPerArrayList = new ArrayList<Integer>();
		for (int i = 0; i < comprovacioRendimentTmp.llistaArrayList.size(); i++) {
			idsPerArrayList.add(i);
		}
		System.out.printf("S'ha inicialitzat la llista idsPerArrayList amb %d elements\n", idsPerArrayList.size());
		System.out.printf("El primer element té el valor: %d\nL'últim element té el valor: %d\n",
				idsPerArrayList.get(0), idsPerArrayList.get(idsPerArrayList.size() - 1));

		System.out.println("---- APARTAT 2 ----");
		for (Integer i : idsPerArrayList) {
			int waypointId = comprovacioRendimentTmp.llistaArrayList.get(i).getId();
			System.out.printf("ABANS DEL CANVI: comprovacioRendimentTmp.llistaArrayList.get(%d).getId(): %d\n", i,
					waypointId);

			comprovacioRendimentTmp.llistaArrayList.get(i).setId(i);

			waypointId = comprovacioRendimentTmp.llistaArrayList.get(i).getId();
			System.out.printf("DESPRÉS DEL CANVI: comprovacioRendimentTmp.llistaArrayList.get(%d).getId(): %d\n\n", i,
					waypointId);
		}

		System.out.println("---- APARTAT 3.1 (bucle for) ----");
		for (Waypoint_Dades w : comprovacioRendimentTmp.llistaArrayList) {
			System.out.printf("ID = %d, nom = %s\n", w.getId(), w.getNom());
		}

		System.out.println();
		System.out.println("---- APARTAT 3.1 (Iterator) ----");
		Iterator<Waypoint_Dades> iter = comprovacioRendimentTmp.llistaArrayList.iterator();
		while (iter.hasNext()) {
			Waypoint_Dades currentWaypoint = iter.next();
			System.out.printf("ID = %d, nom = %s\n", currentWaypoint.getId(), currentWaypoint.getNom());
		}

		System.out.println();
		System.out.println("---- APARTAT 4 ----");
		System.out.printf("Preparat per esborrar el contingut de listaLinkedList que té %d elements\n",
				comprovacioRendimentTmp.llistaLinkedList.size());

		comprovacioRendimentTmp.llistaLinkedList.clear();
		System.out.printf("Esborrada. Ara llistaLinkedList té %d elements\n",
				comprovacioRendimentTmp.llistaLinkedList.size());

		for (Waypoint_Dades w : comprovacioRendimentTmp.llistaArrayList) {
			comprovacioRendimentTmp.llistaLinkedList.add(w);
		}
		System.out.printf("Copiats els elements de llistaArrayList a listaLinkedList que té %d elements\n",
				comprovacioRendimentTmp.llistaLinkedList.size());

		System.out.println("---- APARTAT 5.1 (bucle for) ----");
		for (Waypoint_Dades w : comprovacioRendimentTmp.llistaArrayList) {
			if (w.getId() > 5) {
				w.setNom("Òrbita de Mart");
				System.out.printf("Modificat el waypoint amb id %d\n", w.getId());
			}
		}

		System.out.println();
		System.out.println("---- APARTAT 5.1 (comprovació) ----");
		for (int i = 0; i < comprovacioRendimentTmp.llistaArrayList.size(); i++) {
			System.out.printf("El waypoint amb id = %d té el nom = %s\n",
					comprovacioRendimentTmp.llistaArrayList.get(i).getId(),
					comprovacioRendimentTmp.llistaArrayList.get(i).getNom());
		}

		System.out.println();
		System.out.println("---- APARTAT 5.2 (Iterator) ----");
		Iterator<Waypoint_Dades> i = comprovacioRendimentTmp.llistaArrayList.iterator();

		while (i.hasNext()) {
			Waypoint_Dades w = i.next();
			if (w.getId() < 5) {
				w.setNom("Punt Lagrande entre la Terra i la Lluna");
				System.out.printf("Modificat el waypoint amb id = %d\n", w.getId());
			}

		}

		System.out.println();
		System.out.println("---- APARTAT 5.2 (comprovació) ----");
		i = comprovacioRendimentTmp.llistaArrayList.iterator();
		while (i.hasNext()) {
			Waypoint_Dades w = i.next();
			System.out.printf("El waypoint amb id = %d té el nom = %s\n", w.getId(), w.getNom());
		}

		return comprovacioRendimentTmp;
	}

	public static ComprovacioRendiment esborrarWaypoints(ComprovacioRendiment comprovacioRendimentTmp) {
//		System.out.println("---- APARTAT 1 ----");
//		for(Waypoint_Dades w : comprovacioRendimentTmp.llistaArrayList) {
//			if (w.getId() < 6) {
//				comprovacioRendimentTmp.llistaArrayList.remove(w);
//			}
//		}
//		No deixa esborrar l'element per l'excepcio de modificació de concurrencia. 
//		(https://docs.oracle.com/javase/7/docs/api/java/util/ConcurrentModificationException.html)

		System.out.println("---- APARTAT 2 (Iterator) ----");
		Iterator<Waypoint_Dades> iter = comprovacioRendimentTmp.llistaLinkedList.iterator();
		while (iter.hasNext()) {
			Waypoint_Dades w = iter.next();
			if (w.getId() > 4) {
				System.out.printf("Esborrat el waypoint amb id = %d\n", w.getId());
				iter.remove();
			}
		}

		System.out.println();
		System.out.println("---- APARTAT 2 (comprovació) ----");
		iter = comprovacioRendimentTmp.llistaLinkedList.iterator();
		while (iter.hasNext()) {
			Waypoint_Dades w = iter.next();
			System.out.printf("El waypoint amb id = %d té el nom = %s\n", w.getId(), w.getNom());
		}

		System.out.println();
		System.out.println("---- APARTAT 3 (listIterator) ----");
		ListIterator<Waypoint_Dades> lIter = comprovacioRendimentTmp.llistaLinkedList.listIterator();
		while (lIter.hasNext()) {
			Waypoint_Dades w = lIter.next();
			if (w.getId() == 2) {
				System.out.printf("Esborrat el waypoint amb id = %d\n", w.getId());
				lIter.remove();
			}
		}

		System.out.println();
		System.out.println("---- APARTAT 3 (comprovació) ----");
		while (lIter.hasPrevious()) {
			Waypoint_Dades w = lIter.previous();
			System.out.printf("El waypoint amb id = %d té el nom = %s\n", w.getId(), w.getNom());
		}
		return comprovacioRendimentTmp;
	}

	// !!!!!!! Modificar una copia del arraylist per no afectar a l'original
	public static ComprovacioRendiment modificarCoordenadesINomDeWaypoints(
			ComprovacioRendiment comprovacioRendimentTmp) {
		Scanner sc = new Scanner(System.in);
		for (Waypoint_Dades wp : comprovacioRendimentTmp.llistaArrayList) {
			if (wp.getId() % 2 == 0) {

				System.out.printf("----- Modificaciar el waypoint amb id = %d -----\n", wp.getId());

				// NOM del waypoint
				System.out.printf("Nom actual: %s\n", wp.getNom());
				System.out.printf("Nom nou: ");
				wp.setNom(sc.nextLine());

				System.out.println();

				String newCoordenates = "";
				// Checkers
				int[] arrayCoordenades = new int[3];
				int insertsCounter = 0;

				while (newCoordenates.split(" ").length != 3 || insertsCounter != 3) {
					insertsCounter = 0;
					// COORDENADES del waypoint
					// Pas 1 - Ask for coordinates
					System.out.printf("Coordenades actuals: (%d %d %d)\n", wp.getCoordenades()[0],
							wp.getCoordenades()[1], wp.getCoordenades()[2]);
					System.out.printf("Coordenades noves (format 1 17 7): ");
					newCoordenates = sc.nextLine();

					System.out.println();

					// Pas 2 - Secure 3 inputs with blank spaces.
					if (newCoordenates.split(" ").length != 3) {
						System.out.printf(
								"ERROR: introduir 3 paràmeters separats per 1 espai en blanc. Has introduït %d paràmetres\n",
								newCoordenates.split(" ").length);
					} else {
						// Pas 3 - Check that all 3 inputs are integers
						for (String c : newCoordenates.split(" ")) {
							if (Cadena.singIsInt(c)) {
								arrayCoordenades[insertsCounter] = Integer.parseInt(c);
								insertsCounter++;
							} else {
								System.out.printf("ERROR: coordenada %s no vàlida.\n", c);
							}
						}
					}
				}
				// Pas 4 - Set the coordinates
				wp.setCoordenades(arrayCoordenades);
			}
		}
		return comprovacioRendimentTmp;
	}

	// !!!!!!! Modificar una copia del arraylist per no afectar a l'original
	public static void visualitzarWaypointsOrdenats(ComprovacioRendiment comprovacioRendimentTmp) {
		Collections.sort(comprovacioRendimentTmp.llistaArrayList);
		for (Waypoint_Dades wp : comprovacioRendimentTmp.llistaArrayList) {
			System.out.println(wp);
		}
	}

	public static void waypointsACertaDistanciaMaxDeLaTerra(ComprovacioRendiment comprovacioRendimentTmp) {
		Scanner sc = new Scanner(System.in);
		String userDistanceString = "";
		while (!Cadena.singIsInt(userDistanceString)) {
			System.out.print("Distància màxima de la terra: ");
			userDistanceString = sc.nextLine();
		}
		int userDistance = Integer.parseInt(userDistanceString);
		for (Waypoint_Dades wp : comprovacioRendimentTmp.llistaArrayList) {
			if (Waypoint.distance(wp.getCoordenades()) <= userDistance) {
				System.out.println(wp);
			}
		}
	}

	public static String formatDate(LocalDateTime date) {
		if (date != null) {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
			return date.format(formatter);
		}
		return "NULL";
	}

	public static int distance(int[] coordinates) {
		int distance = 0;
		for (int co : coordinates) {
			distance += Math.pow(co, 2);
		}
		return distance;
	}

	// EXERCICI 6
	public static ComprovacioRendiment inicialitzarDadesMaypoints(ComprovacioRendiment comprovacioRendimentTmp) {

		// Clear list
		if (comprovacioRendimentTmp.llistaWaypoints.size() != 0) {
			int waypointsToDelete = comprovacioRendimentTmp.llistaWaypoints.size();

			for (int i = waypointsToDelete; i > 0; i--) {
				comprovacioRendimentTmp.llistaWaypoints.remove(i);
			}
		}

		// Insert elements
		comprovacioRendimentTmp.llistaWaypoints.add(new Waypoint_Dades(0, "Òrbita de la Terra", new int[] { 0, 0, 0 },
				true, LocalDateTime.parse("21-10-2020 01:11", Data.formatter),
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter), null, 0));

		comprovacioRendimentTmp.llistaWaypoints.add(new Waypoint_Dades(1, "Punt Lagrange Terra-Lluna",
				new int[] { 1, 1, 1 }, true, LocalDateTime.parse("21-10-2020 01:00", Data.formatter),
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter), null, 6));

		comprovacioRendimentTmp.llistaWaypoints.add(new Waypoint_Dades(2, "Òrbita de la Lluna", new int[] { 2, 2, 2 },
				true, LocalDateTime.parse("21-10-2020 00:50", Data.formatter),
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter), null, 1));

		comprovacioRendimentTmp.llistaWaypoints.add(new Waypoint_Dades(3, "Òrbita de la de Mart", new int[] { 3, 3, 3 },
				true, LocalDateTime.parse("21-10-2020 00:40", Data.formatter),
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter), null, 0));

		comprovacioRendimentTmp.llistaWaypoints.add(new Waypoint_Dades(4, "Òrbita de Júpiter", new int[] { 4, 4, 4 },
				true, LocalDateTime.parse("21-10-2020 00:30", Data.formatter),
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter), null, 0));

		comprovacioRendimentTmp.llistaWaypoints.add(new Waypoint_Dades(5, "Punt Lagrange Júpiter-Europa",
				new int[] { 5, 5, 5 }, true, LocalDateTime.parse("21-10-2020 00:20", Data.formatter),
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter), null, 6));

		comprovacioRendimentTmp.llistaWaypoints.add(new Waypoint_Dades(6, "Òrbita de Europa", new int[] { 6, 6, 6 },
				true, LocalDateTime.parse("21-10-2020 00:10", Data.formatter),
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter), null, 0));

		comprovacioRendimentTmp.llistaWaypoints.add(new Waypoint_Dades(7, "Òrbita de Venus", new int[] { 7, 7, 7 },
				true, LocalDateTime.parse("21-10-2020 00:01", Data.formatter),
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter), null, 0));

		return comprovacioRendimentTmp;
	}

	// EXERCICI 7
	public static ComprovacioRendiment nouWaypoint(ComprovacioRendiment comprovacioRendimentTmp) {
		// Variables
		Scanner sc = new Scanner(System.in);

		int idNewWaypoint = 0;

		String nameNewWaypoint = "";

		String coordenatesNewWaypoint = "";
		int insertsCounter = 0;
		int[] arrayCoordenatesNewyWaypoint = new int[3];

		boolean activeNewWaipoint = true;
		String sActive = "";

		LocalDateTime currentDateNewWaypoint = LocalDateTime.now();
		String anulationDateNewWaypoint = "";
		int tipus = -1;

		// -- ID --
		// Search the bigger id and save it
		for (Waypoint_Dades w : comprovacioRendimentTmp.llistaWaypoints) {
			if (w.getId() > idNewWaypoint) {
				idNewWaypoint = w.getId();
			}
		}
		// Add one to the id
		idNewWaypoint++;

		// -- WAYPOINT NAME --
		System.out.printf("Nom del waypoint: ");
		nameNewWaypoint = sc.nextLine();

		// -- COORDENATES --
		while (coordenatesNewWaypoint.split(" ").length != 3 || insertsCounter != 3) {
			insertsCounter = 0;
			// COORDENADES del waypoint
			// Ask for coordinates
			System.out.printf("Coordenades noves (format 1 17 7): ");
			coordenatesNewWaypoint = sc.nextLine();

			System.out.println();

			// Secure 3 inputs with blank spaces.
			if (coordenatesNewWaypoint.split(" ").length != 3) {
				System.out.printf(
						"ERROR: introduir 3 paràmeters separats per 1 espai en blanc. Has introduït %d paràmetres\n",
						coordenatesNewWaypoint.split(" ").length);
			} else {
				// Check that all 3 inputs are integers
				for (String c : coordenatesNewWaypoint.split(" ")) {
					if (Cadena.singIsInt(c)) {
						arrayCoordenatesNewyWaypoint[insertsCounter] = Integer.parseInt(c);
						insertsCounter++;
					} else {
						System.out.printf("ERROR: coordenada %s no vàlida.\n", coordenatesNewWaypoint);
					}
				}
			}
		}

		// -- ACTIVE --
		Collator comparador = Collator.getInstance();
		comparador.setStrength(Collator.PRIMARY);
		// While the input isn't correct
		while (!(comparador.equals(sActive, "true")) && !(comparador.equals(sActive, "false"))) {
			System.out.printf("Actiu? (format: true|false): ");
			sActive = sc.nextLine();
			if ((comparador.compare(sActive, "true")) == 0 || (comparador.compare(sActive, "false")) == 0) {
				activeNewWaipoint = Boolean.parseBoolean(sActive);
			} else {
				System.out.printf("ERROR: actiu %s no vàlid.\n", sActive);
			}
		}

		// -- DELETION DATE --

		int insertsCounterDate = 0;
		while (insertsCounterDate != 3) {
			insertsCounterDate = 0;
			System.out.printf("Data d'anulació: (DD-MM-AAAA)");
			anulationDateNewWaypoint = sc.nextLine();
			if (Data.esData(anulationDateNewWaypoint)) {
				for (String s : anulationDateNewWaypoint.split("-")) {
					if (isInteger(s)) {
						insertsCounterDate++;
					} else {
						System.out.printf("ERROR: date de creació %s no vàlida.\n", anulationDateNewWaypoint);
					}
				}
			} else {
				System.out.printf("ERROR: date de creació %s no vàlida.\n", anulationDateNewWaypoint);
			}
		}

		// -- TYPE --
		System.out.println("Tipus de waypoints disponibles");
		for (int i = 0; i < TipusWaypoint.TIPUS_WAYPOINT.length; i++) {
			System.out.printf("%d: %s\n", i, TipusWaypoint.TIPUS_WAYPOINT[i]);
		}
		while (tipus < 0 && tipus > TipusWaypoint.TIPUS_WAYPOINT.length - 1) {
			System.out.printf("Tipus de waypoint (format: 3)");
			tipus = sc.nextInt();
			if (tipus < 0 && tipus > TipusWaypoint.TIPUS_WAYPOINT.length - 1) {
				System.out.printf(
						"ERROR: Introduir 1 numero dels visualitzats en la llista anterior. El numero %d no existeix",
						tipus);
			}
		}

		// Add Waypoint
		comprovacioRendimentTmp.llistaWaypoints.add(new Waypoint_Dades(idNewWaypoint, nameNewWaypoint,
				arrayCoordenatesNewyWaypoint, activeNewWaipoint, currentDateNewWaypoint,
				LocalDateTime.parse((anulationDateNewWaypoint + " 00:00"), Data.formatter), currentDateNewWaypoint,
				tipus));

		return comprovacioRendimentTmp;
	}

	// EXERCICI 8
	public static void waypointsVsTipus(ComprovacioRendiment comprovacioRendimentTmp) {
		// Variable
		Scanner sc = new Scanner(System.in);
		int tipusDesitjat = -1;
		// Create lists
		LinkedList<Waypoint_Dades> llistaA = new LinkedList<Waypoint_Dades>();
		LinkedList<Waypoint_Dades> llistaB = new LinkedList<Waypoint_Dades>();

		// Insert elements in list A
		for (Waypoint_Dades w : comprovacioRendimentTmp.llistaWaypoints) {
			llistaA.add(w);
		}
		// Show avaible waypoints
		System.out.println("Tipus de waypoints disponibles");
		for (int i = 0; i < TipusWaypoint.TIPUS_WAYPOINT.length; i++) {
			System.out.printf("%d: %s\n", i, TipusWaypoint.TIPUS_WAYPOINT[i]);
		}
		System.out.printf("Tipus de waypoint (format: 3)");
		tipusDesitjat = sc.nextInt();

		// Remove from list A insert in B
		for (int i = 0; i < llistaA.size(); i++) {
			if (llistaA.get(i).getTipus() == tipusDesitjat) {
				System.out.printf("Esborrat el waypoint amb id = %d del tipus %d. Waypoint llistaA --> llistaB\n",
						llistaA.get(i).getId(), llistaA.get(i).getTipus());
				llistaB.add(llistaA.get(i));
				llistaA.remove(i);
			}
		}

		// Show list B
		System.out.printf("\nLlistaB de waypoints del tipus %d:\n", tipusDesitjat);
		for (Waypoint_Dades w : llistaB) {
			System.out.printf("id %d: %s, de tipus %d", w.getId(), w.getNom(), w.getTipus());
		}
	}

	// EXERCICI 9
	public static void numWaypointsVsTipus(ComprovacioRendiment comprovacioRendimentTmp) {
		Set<Waypoint_Dades> s = new TreeSet<Waypoint_Dades>();
		HashMap<Integer, Waypoint_Dades> hm = new HashMap<Integer, Waypoint_Dades>();
		for (Waypoint_Dades w : comprovacioRendimentTmp.llistaWaypoints) {
			s.addAll(comprovacioRendimentTmp.llistaWaypoints);
		}
			
		for (Waypoint_Dades w : s) {
			comprovacioRendimentTmp.mapaWaypoints.put(w.getTipus(), w);
		}
	}

	// EXERCICI 10
	public static void trobarWaypointsVsNom(ComprovacioRendiment comprovacioRendimentTmp) {
		Scanner sc = new Scanner(System.in);
		String userSearch = ""; 
		Collator comparador = Collator.getInstance();
		comparador.setStrength(Collator.SECONDARY);
		System.out.println("Nom que voleu buscar: ");
		userSearch = sc.nextLine().toLowerCase();
		for (Waypoint_Dades w : comprovacioRendimentTmp.llistaWaypoints) {
			String currentWaypointName = w.getNom().toLowerCase();
			if (currentWaypointName.indexOf(userSearch) != -1) {
				System.out.printf("id %d: %s\n", w.getId(), w.getNom());
			}
		}
			
	}

	// EXERCICI 11
	public static void ordenarWaypointsPerData(ComprovacioRendiment comprovacioRendimentTmp) {
		Collections.sort(comprovacioRendimentTmp.llistaWaypoints);
		
		System.out.println("Ordre per data: ");
		for (Waypoint_Dades w : comprovacioRendimentTmp.llistaWaypoints) {
			System.out.println(w.toString());
		}
		
	}

	public static boolean isInteger(String s) {
		try {
			Integer.parseInt(s);
		} catch (NumberFormatException e) {
			return false;
		} catch (NullPointerException e) {
			return false;
		}
		// only got here if we didn't return false
		return true;
	}
}
