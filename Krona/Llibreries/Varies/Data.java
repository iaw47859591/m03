package Varies;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Data {

	public static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
	
	public static String imprimirData(LocalDateTime dataTmp) {
		if (dataTmp.equals(null)) {
			return null;
		}
		
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		
		String dateFormated = df.format(df);
		
		return dateFormated;
	}
	
	public static boolean esData(String dataTmp) {
		if ((dataTmp.charAt(2) == '-') && (dataTmp.charAt(5) == '-')) {
			return true;
		}
		return false;
	}	
	
}
