package Llibreries;

public final class Cadena {

	public static boolean singIsInt(String cadena) {
		try {
			Integer.parseInt(cadena);
			return true;
		} catch (NumberFormatException nfe) {
			return false;
		}
	}
	
}
