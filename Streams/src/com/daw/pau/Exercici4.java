package com.daw.pau;

import java.io.File;

public class Exercici4 {
	public static void main(String[] args) {
		// Directory to make backups
		File d = new File("Backup/Things");
		
		File currentFile;
		// Number of backups
		int backUpCounter = 0;
		// Base name for new directory
		String baseName = "backUp";
		String directoyName = baseName + backUpCounter;

		try {
			File dBackUp = new File("Backup/Things" + directoyName);
			do {
				backUpCounter++;
				directoyName = baseName + backUpCounter;
				dBackUp = new File("Backup/Things" + directoyName);
			} while (dBackUp.exists());
			
			dBackUp.mkdir();

			String[] filesList = d.list();
			for (int i = 0; i < filesList.length; i++) {
				currentFile = new File(filesList[i]);
			}
		} catch (Exception e) {
			System.out.println("Something went wrong");
			System.out.println(e.getMessage());
		}
	}
}
