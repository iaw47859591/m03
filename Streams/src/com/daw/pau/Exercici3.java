package com.daw.pau;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Exercici3 {
	public static void main(String[] args) throws IOException {
		// Get the files
		File f1 = new File ("ex3_1.txt");
		File f2 = new File ("ex3_2.txt");
		// Entry
		FileReader fr1 = null;
		FileReader fr2 = null;
		// Buffers
		BufferedReader br1;
		BufferedReader br2;
		// Lines to get
		String lineFileOne = "";
		String lineFileTwo = "";
		// Line counter
		int lineCounter = 0; 
		try {
			// Initiate the file entry
			fr1 = new FileReader(f1);
			fr2 = new FileReader(f2);
			// Initiate the buffer
			br1 = new BufferedReader(fr1);
			br2 = new BufferedReader(fr2);
			// Loop through the lines
			while (((lineFileOne = br1.readLine()) != null) && ((lineFileTwo = br2.readLine()) != null)) {
				lineCounter++;
				if (!lineFileOne.equals(lineFileTwo)) {
					System.out.println("Error in line: " + lineCounter);
					System.out.printf("\tF1: %s\n\tF2: %s\n", lineFileOne, lineFileTwo);
				}
				
			}
			// If there are still lines in file one
			if ((lineFileOne = br1.readLine()) != null) {
				lineCounter++;
				System.out.println("\nThe File 1 has more lines\n");
				System.out.println("Line: " + lineCounter);
				System.out.println("\t" + lineFileOne);
				// While there are still lines
				while ((lineFileOne = br1.readLine()) != null) {
					lineCounter++;
					System.out.println("\nThe File 1 has more lines\n");
					System.out.println("Line: " + lineCounter);
					System.out.println("\t" + lineFileOne);
				}
			}
			// If there are still lines in file two
						if ((lineFileTwo = br2.readLine()) != null) {
							lineCounter++;
							System.out.println("\nThe File 2 has more lines\n");
							System.out.println("Line: " + lineCounter);
							System.out.println("\t" + lineFileTwo);
							// While there are still lines
							while ((lineFileTwo = br2.readLine()) != null) {
								lineCounter++;
								System.out.println("Line: " + lineCounter);
								System.out.println("\t" + lineFileTwo);
							}
						}
			
			
			
			
		} catch ( FileNotFoundException e ) {
			System.out.println("The file does not exist");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		} finally {
			if (fr1 != null) fr1.close();
			if (fr2 != null) fr2.close();
		}
			   
	}
}
